# FUND-JOB-7, HRC-PAVILION
This repository (along with the fund-base companion) holds the source code for the fund job 7, and hrc-pavilion apps.

### How do I get set up? ###

This is an Android Studio Project

To Set it Up:
	1. Initialize the fund-base submodule
	2. make sure the same branches of fund-app and fund-base are checked out
	
For Fund Job 7:
	1. Checkout the branch names "FUND-JOB-7" for both fund-app and fund-base
	
For Fund Job 7: