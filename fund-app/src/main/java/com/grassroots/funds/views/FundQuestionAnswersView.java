package com.grassroots.funds.views;

import java.util.List;

import org.apache.log4j.Logger;

import com.grassroots.funds.R;
import com.grassroots.petition.GlobalData;
import com.grassroots.petition.models.Question;
import com.grassroots.petition.models.QuestionAnswer;
import com.grassroots.petition.models.SubjectAnswer;
import com.grassroots.petition.views.QuestionAnswersView;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class FundQuestionAnswersView extends QuestionAnswersView {

	private static final String TAG = "XENLOG";
	private static final Logger LOGGER = Logger.getLogger(TAG);

	private int MAX_COLUMN_SIZE = 4;

	/**
	 * Default constructor
	 * 
	 * @param context
	 */
	public FundQuestionAnswersView(Context context) {
		super(context);
	}

	/**
	 * Default constructor
	 * 
	 * @param context
	 * @param attrs
	 */
	public FundQuestionAnswersView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	/**
	 * Default constructor
	 * 
	 * @param context
	 * @param attrs
	 * @param defStyle
	 */
	public FundQuestionAnswersView(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public void setQuestion(Question question) {
        WebView webview = ((GlobalData) (GlobalData.getContext())).getWebview();
		
		LinearLayout webViewParent = (LinearLayout) findViewById(R.id.webviewParent);
		webview = ((GlobalData) (GlobalData.getContext())).getWebview();
		if (webview.getParent() != null) {
			((ViewGroup) webview.getParent()).removeAllViews();
		}
		webViewParent.setVisibility(View.GONE);
		webview.setVisibility(View.GONE);
		
		SubjectAnswer subjectAnswer = null;
		if (null != question) {
			subjectAnswer = petitionAnswers.getAnswerTo(question);
		}
		View view =  findViewById(R.id.question_scrollview);
		
		Question.Type type = question.getType();
		List<QuestionAnswer> answers = question.getAnswers();
		View twoColumns = findViewById(R.id.multiple_choice_2_column);
		RadioGroup firstColumn = (RadioGroup) findViewById(R.id.multiple_choice_1);
		RadioGroup secondColumn = (RadioGroup) findViewById(R.id.multiple_choice_2);
		firstColumn.removeAllViewsInLayout();
		secondColumn.removeAllViewsInLayout();
		if (Question.Type.MULTIPLE == type && answers.size() > MAX_COLUMN_SIZE) {
			currentQuestion = question;
			int checkedAnswerId = SubjectAnswer.NO_VALUE;
			if (null != subjectAnswer) {
				checkedAnswerId = subjectAnswer.getAnswerId();
			}
			RadioGroup multiChoiceGroup = (RadioGroup) findViewById(R.id.multiple_choice);
			multiChoiceGroup.removeAllViewsInLayout();
			multiChoiceButtonIdsToAnswers.clear();
			multiChoiceGroup.setVisibility(View.GONE);
			twoColumns.setVisibility(View.VISIBLE);
			Log.e("answers size", "sk answers size : "+answers.size());
			if(answers.size() > (MAX_COLUMN_SIZE * 2)){
				MAX_COLUMN_SIZE = answers.size()/2;
				Log.e("MAX_COLUMN_SIZE size", "sk MAX_COLUMN_SIZE size : "+MAX_COLUMN_SIZE);
			}
			for (int i = 0; i < answers.size(); i++) {
				QuestionAnswer answerChoice = answers.get(i);
				RadioButton button = (RadioButton) inflate(getContext(),
						R.layout.question_radio_button, null);
				button.setText(answerChoice.getValue());
				if (i < MAX_COLUMN_SIZE) {
					firstColumn.addView(button);
				} else {
					secondColumn.addView(button);
				}
				multiChoiceButtonIdsToAnswers.put(button.getId(), answerChoice);
				LOGGER.warn("multiChoiceButtonIdsToAnswers.put("
						+ button.getId() + "," + answerChoice.getValue() + ")");
				if (answerChoice.getId() == checkedAnswerId) {
					button.setChecked(true);
				}
			}
			if (view != null) {
				LOGGER.warn("In FundQuestionAnswersView question_scrollview is not null");
				LinearLayout.LayoutParams layoutParam = new LinearLayout.LayoutParams(
						LinearLayout.LayoutParams.WRAP_CONTENT,
						LinearLayout.LayoutParams.WRAP_CONTENT);
				view.setLayoutParams(layoutParam);
			}else{
				LOGGER.warn("In FundQuestionAnswersView question_scrollview is null");
			}
		} else {
			twoColumns.setVisibility(View.GONE);
			super.setQuestion(question);
		}

	}

	@Override
	public SubjectAnswer getAnswer() {
		Question.Type type = currentQuestion.getType();
		List<QuestionAnswer> answers = currentQuestion.getAnswers();
		if (Question.Type.MULTIPLE == type && answers.size() > MAX_COLUMN_SIZE) {
			RadioGroup firstColumn = (RadioGroup) findViewById(R.id.multiple_choice_1);
			RadioGroup secondColumn = (RadioGroup) findViewById(R.id.multiple_choice_2);
			SubjectAnswer answer = new SubjectAnswer();
			answer.setQuestionId(currentQuestion.getId());
			int checked = firstColumn.getCheckedRadioButtonId();
			if (checked == -1) {
				checked = secondColumn.getCheckedRadioButtonId();
			}
			LOGGER.warn("In FundQuestionAnswersView:getAnswer() checked = "
					+ checked);
			QuestionAnswer questionAnswer = multiChoiceButtonIdsToAnswers
					.get(checked);
			if (questionAnswer != null) {
				answer.setAnswerId(questionAnswer.getId());
			} else {
				LOGGER.warn("In FundQuestionAnswersView:getAnswer() questionAnswer is null");
			}
			return answer;
		} else {
			return super.getAnswer();
		}
	}

}
