package com.grassroots.funds.activities;

import java.util.List;

import org.apache.log4j.Logger;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.actionbarsherlock.app.ActionBar;
import com.grassroots.petition.R;
import com.grassroots.petition.activities.BaseWalklistActivity;
import com.grassroots.petition.activities.TallyActivity;
import com.grassroots.petition.activities.WalklistDetailsActivity;
import com.grassroots.petition.db.helper.WalklistDataSource;
import com.grassroots.petition.fragments.WalklistDetailsFragment;
import com.grassroots.petition.fragments.WalklistListFragment;
import com.grassroots.petition.fragments.WalklistMapFragment;
import com.grassroots.petition.fragments.WalklistTallyFragment;
import com.grassroots.petition.models.KnockStatus;
import com.grassroots.petition.models.Subject;
import com.grassroots.petition.models.Walklist;

import hugo.weaving.DebugLog;

public class FundWalklistActivity extends BaseWalklistActivity {
	
	
	private FrameLayout mapFragmentHolder, detailsFragmentHolder, tallyFragmentHolder;
    private RelativeLayout titlelayout;
    public boolean isTablet = false;
    private FragmentManager fragmentManager = null;
    private WalklistMapFragment mapFragment = null;
    private WalklistDetailsFragment detailsFragment = null;
    private WalklistListFragment listFragment = null;
    private WalklistTallyFragment tallyFragment = null;

    private Walklist walklist = null;
    private ActionBar.Tab listTab = null;
    private ActionBar.Tab mapTab = null;
    private Subject selectedSubject = null;
    private ActionBar actionBar = null;
	public boolean isLocationList;

    public static final String WALKLIST_EXTRA_PARAMETER_SUBJECT = "SELECTED_SUBJECT_EXTRA";
    public static final String SHOULD_SHOW_MAP_TAB = "SHOULD_SHOW_MAP_TAB";
    private static final String TAG = BaseWalklistActivity.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);
    
    private String selectedLastName = null;
    public static final String WALKLIST_EXTRA_PARAMETER_LASTNAME="SELECTED_LASTNAME_EXTRA";


    @Override @DebugLog
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.walklistscreen);
    }
    
    private void switchToDetails() {
        hideMapFragment();
        loadDetailsFragment();
    }
    /**
     * show the reports for the user
     */
    private void showTallyScreen(){
        Intent intent = new Intent(FundWalklistActivity.this,TallyActivity.class);
        startActivity(intent);
    }
    
    private void hideListFragment() {
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.hide(listFragment);
        ft.commit();
    }

    private void showListFragment() {
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.show(listFragment);
        ft.commit();
    }

    private void hideMapFragment() {
        if(mapFragment != null){
        getGlobalData().setZoomLevel(mapFragment.getMapView().getZoomLevel());
        getGlobalData().setMapCenterLatitude(
                mapFragment.getMapView().getMapCenter().getLatitudeE6());
        getGlobalData().setMapCenterLongitude(
                mapFragment.getMapView().getMapCenter().getLongitudeE6());
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.hide(mapFragment);
        ft.commit();
        }
    }
    
    private void loadDetailsFragment() {

        detailsFragment = new WalklistDetailsFragment();
        detailsFragment.setViewListener(detailsFragmentListener);
        detailsFragment.setWalklist(walklist);
        detailsFragment.setPetition(petition);

        FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction();
        fragmentTransaction.replace(R.id.details_fragment_container,
                detailsFragment);
        fragmentTransaction.commit();
        fragmentManager.executePendingTransactions();

        if(isLocationList){
        	if(selectedLastName == null){
        		//selectedLastName = listFragment.lastNamesFromDatabase.get(0);

        	}
        	String knockvalue = null;
        	if(getGlobalData().getKnockStatusFilter() != null && getGlobalData().getKnockStatusFilter().length()>0){
        	KnockStatus storedKnock = KnockStatus.valueOf(getGlobalData().getKnockStatusFilter());
        	
        	if(storedKnock!=null){
        		knockvalue = ""+storedKnock.getRepresentation();
        	}
        	
        	detailsFragment.setSubjects(new WalklistDataSource(this).getWalklistForLastName(knockvalue, selectedLastName));
        	}else{
            	detailsFragment.setSubjects(new WalklistDataSource(this).getWalklistForLastName(selectedLastName));

        	}
        	listFragment.onSelectLastName(selectedLastName);

        }else{
        detailsFragment.setSubjects(walklist
                .getSameAddressSubjects(selectedSubject));
        }
        mapFragmentHolder.setVisibility(View.GONE);
        tallyFragmentHolder.setVisibility(View.GONE);
        detailsFragmentHolder.setVisibility(View.VISIBLE);

    }
    
    /**
     * Filtes subjects with lastname
     * 
     * @param lastnamefilter
     */
    private void updateWalklistWithLastName(String lastnamefilter) {
    	if(lastnamefilter != null){
    	 List<Subject> addresses = listFragment.setLastNameFilter(lastnamefilter);
    	 if(detailsFragment != null){
    	     detailsFragment.setSubjects(addresses);
    	 }
    	}
    }
    
    /**
     * Filtes subjects with color
     * 
     * @param color filter
     */
    private void updateWalklistWithColor(String colorFilter) {
    	if(colorFilter != null){
    	 List<Subject> addresses = listFragment.setColorFilter(colorFilter);
    	 if(detailsFragment != null){
    	     detailsFragment.setSubjects(addresses);
    	 }
    	}
    }
    private void loadTallyFragment(){
    	FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction();
        fragmentTransaction.replace(R.id.tally_fragment_container, tallyFragment);
        fragmentTransaction.commit();
        if (!isTablet) {
            hideListFragment();
        }
        fragmentManager.executePendingTransactions();
        mapFragmentHolder.setVisibility(View.GONE);
        detailsFragmentHolder.setVisibility(View.GONE);
        tallyFragmentHolder.setVisibility(View.VISIBLE);
        
    }
    private void loadMapFragment() {
        mapFragment = new WalklistMapFragment();
        mapFragment.setViewListener(mapFragmentListener);
        mapFragment.setWalklist(walklist);
        mapFragment.restoreMapConfig(getGlobalData().getMapCenterLatitude(),
                getGlobalData().getMapCenterLongitude(), getGlobalData()
                        .getZoomLevel());
        FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction();
        fragmentTransaction.replace(R.id.map_fragment_container, mapFragment);
        fragmentTransaction.commit();
        if (!isTablet) {
            hideListFragment();
        }
        fragmentManager.executePendingTransactions();
        mapFragmentHolder.setVisibility(View.VISIBLE);
        detailsFragmentHolder.setVisibility(View.GONE);
    }
    

    /**
     * Show subject details in the new activity of class
     * 'WalklistDetailsActivity'
     * 
     * @param subject
     *            - a subject object which contains address and will be showed
     *            in details activity.
     */
    private void showSubjectDetails(Subject subject) {
        if (null != subject) {
            selectedSubject = subject;
            selectedLastName = subject.getLastName();
            Intent intent = new Intent();
            intent.setClass(this, WalklistDetailsActivity.class);
            if(isLocationList){
            	intent.putExtra(WALKLIST_EXTRA_PARAMETER_LASTNAME, selectedLastName);
            }else{
                intent.putExtra(WALKLIST_EXTRA_PARAMETER_SUBJECT, subject);
            }
            startActivity(intent);
        }
    }
    
    private void showSubjectDetailsForLastname(String lastname) {
    	if (null != lastname) {
            Intent intent = new Intent();
            intent.setClass(this, WalklistDetailsActivity.class);
            intent.putExtra(WALKLIST_EXTRA_PARAMETER_LASTNAME, lastname);
            startActivity(intent);
        }
		
	}
	
	/**
     * List fragment listener, used to receive events from the list with
     * subjects addresses
     */
    private WalklistListFragment.ViewListener listFragmentListener = new WalklistListFragment.ViewListener() {
        @Override
        public void onSelectSubject(Subject subject) {
            selectedSubject = subject;
            if (isTablet) {
                switchToDetails();
            } else {
                showSubjectDetails(selectedSubject);
            }
        }

        @Override
        public void onFilterStreetName(String streetNameFilter) {
             updateWalklistWithStreetName(streetNameFilter);
        }

        @Override
        public void onShowHouseNumber(
                Walklist.HouseNumberFilter houseNumberFilter) {
            updateWalklistWithHouseNumber(houseNumberFilter);
        }

        @Override
        public void onFilterKnockStatus(KnockStatus knockStatus) {
            updateWalklistWithKnockStatus(knockStatus);
        }

        @Override
        public void onNewRecord() {
            beginProcessSubject(new Subject());
        }

		@Override
		public void onFilterLastName(String filter) {
			updateWalklistWithLastName(filter);
			
		}

		@Override
		public void onSelectLastName(String lastname) {
			selectedLastName = lastname;
			if (isTablet) {
                switchToDetails();
            } else {
                showSubjectDetailsForLastname(lastname);
            }
		}

        @Override
        public void onTally() {
            showTallyScreen();
        }

		@Override
		public void onFilterColor(String filter) {
			updateWalklistWithColor(filter);
		}

    };
    
    /**
     * Filtes subjects addresses list and markers on map by specified house
     * number.
     * 
     * @param houseNumberFilter
     *            - house number to filter address list
     */
    private void updateWalklistWithHouseNumber(
            Walklist.HouseNumberFilter houseNumberFilter) {
        List<Subject> addresses = listFragment.setHouseNumberFilter(houseNumberFilter);
        if (null != mapFragment) {
            mapFragment.setHouseNumberFilter(houseNumberFilter);
        }
        if(isLocationList){
        	detailsFragment.setSubjects(addresses);
        }
    }
    
    /**
     * Filters subjects addresses list and markers on map by address which
     * should contain specified part of street name.
     * 
     * @param streetNameFilter
     *            - part of street name to filter subject addresses.
     */
    private void updateWalklistWithStreetName(String streetNameFilter) {
        List<Subject> addresses = listFragment.setStreetNameFilter(streetNameFilter);
        if (null != mapFragment) {
            mapFragment.setStreetNameFilter(streetNameFilter);
        }
        if(detailsFragment != null){
        	if(addresses.size()>0 && streetNameFilter != null && streetNameFilter.length()>0){
   	       selectedSubject = addresses.get(0);
   	       loadDetailsFragment();
        	}
   	     }
    }
    
    /**
     * Filtes subjects addresses list and markers on map by specified knock
     * status which is set to whole address.
     * 
     * @param knockStatus
     *            - knock status which is used to filter address list
     */
    private void updateWalklistWithKnockStatus(KnockStatus knockStatus) {
        List<Subject> addresses = listFragment.setKnockStatusFilter(knockStatus);
        if (null != mapFragment) {
            mapFragment.setKnockStatusFilter(knockStatus);
        }
        if(isLocationList){
            if(detailsFragment != null && detailsFragment.isVisible()){
            	if(knockStatus != null){
                	String knockvalue = ""+knockStatus.getRepresentation();
                	detailsFragment.setSubjects(new WalklistDataSource(this).getWalklistForLastName(knockvalue, ""));
                	if(addresses.size()>0){
                		listFragmentListener.onSelectLastName(addresses.get(0).getLastName());
                	}
                }else{
                    detailsFragment.setSubjects(addresses);
                }

            }
        }else{
        	if(detailsFragment != null && detailsFragment.isVisible()){
        		if(addresses.size()>0 && knockStatus != null){
            		listFragmentListener.onSelectSubject(addresses.get(0));
            	}
        	}
        }
    }    
    

    /**
     * Details fragment listener, used to receive events from the details
     * fragment which contains details information about subject address
     */
    private WalklistDetailsFragment.ViewListener detailsFragmentListener = new WalklistDetailsFragment.ViewListener() {
        @Override
        public void onSetHomeStatus() {
            FundWalklistActivity.this.onSetHomeStatus();
        }

        @Override
        public void onWalklistUpdate() {
            listFragment.refreshWalklist();
            listFragment.onSelectSubject(selectedSubject);
        }

        @Override
        public void onSurveyStart(Subject subject) {
        	FundWalklistActivity.this.beginProcessSubject(subject);
        }

        @Override
        public void onMarketing(Subject subject) {
        	Log.e("get subject","onmarketing : "+subject);
        	getIntent().putExtra(WALKLIST_EXTRA_PARAMETER_SUBJECT, subject);
        	FundWalklistActivity.this.onMarketing();
        }

        @Override
        public void onShowMap() {
            loadMapFragment();
        }

        @Override
        public void onSelectSubject(Subject subject) {
            listFragment.onSelectSubject(subject);
        }
        
        @Override
		public void onFilterLastName(String filter) {
			updateWalklistWithLastName(filter);
			
		}
        @Override
        public void onSelectLastName(String lastname) {
           listFragment.onSelectLastName(lastname); 
        }

		@Override
		public void onShowTally() {
			// TODO Auto-generated method stub
			loadTallyFragment();
			
		}

		@Override
		public void onNewRecord() {
			FundWalklistActivity.this.onMarketing();
		}

    };
    
    /**
     * Map fragment listener, used to receive events from map fragment
     */
    private WalklistMapFragment.ViewListener mapFragmentListener = new WalklistMapFragment.ViewListener() {
        @Override
        public void onSetStatus() {
            FundWalklistActivity.this.onSetHomeStatus();
        }

        @Override
        public void onLogout() {
        	FundWalklistActivity.this.showLogoutDialog();
        }

        @Override
        public void onManualEntry() {
        	FundWalklistActivity.this.beginProcessSubject(new Subject());
        }

        @Override
        public void onMarketing() {
        	FundWalklistActivity.this.onMarketing();
        }

        @Override
        public void onSelectSubject(Subject subject) {
            selectedSubject = subject;
            listFragment.onSelectSubject(subject);
        }

		@Override
		public void onShowTally() {
			loadTallyFragment();
		}

        @Override
        public void onNewRecord() {
            FundWalklistActivity.this.onMarketing();
        }
    };
}
