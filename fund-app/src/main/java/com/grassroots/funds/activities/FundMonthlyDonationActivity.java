package com.grassroots.funds.activities;

import android.app.AlertDialog;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView.OnEditorActionListener;
import android.widget.TextView;

import com.grassroots.funds.R;
import com.grassroots.petition.models.Cart;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FundMonthlyDonationActivity extends FundDonationActivity {

	private Button nextButton,backButton = null;
	private RadioGroup paymentAmountRadioGroup = null,
			paymentTypeRadioGroup = null;
	private EditText amountEditText = null;



	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.monthly_donation_activity);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


		setRecurring("MONTHLY");
		setisAnalog(false);
		TextView headerTitle = (TextView) findViewById(R.id.header_title);
		if (null != headerTitle) {
			headerTitle.setText("Monthly Contribution");
		}

		getDonationAmountJson();

		if(!isHrcPayment) {
			LinearLayout layout = (LinearLayout) findViewById(R.id.monthly_frequency);
			layout.setVisibility(View.GONE);
		}

		backButton = (Button) findViewById(R.id.back_button);
		backButton.setOnClickListener(v -> onBackPressed());
		nextButton = (Button) findViewById(R.id.next_button);
		nextButton.setOnClickListener(v -> {
            RadioButton otherRadioButton = (RadioButton) findViewById(R.id.radiobutton_payment_amount_other);

            float amount = 0;

            if(otherRadioButton.isChecked())
            {
                try
                {
                    amount  = Float.parseFloat(amountEditText.getText()
                            .toString());
                }
                catch (NumberFormatException e)
                {
                    Toast.makeText(FundMonthlyDonationActivity.this,
                            "Not a valid donation format.", Toast.LENGTH_LONG).show();
                    return;
                }
            }

            if ((otherRadioButton.isChecked() && amountEditText.getText() != null)
                    && (amountEditText.getText().toString().length() != 0)
                    && (Float.parseFloat(amountEditText.getText()
                            .toString()) > 0)) {

                //implement this in base(GRU) app.
                if(amount > 999999.99)
                {
                    Toast.makeText(FundMonthlyDonationActivity.this,
                            "Donation must be less than $1 million.", Toast.LENGTH_LONG).show();
                    return;
                }
                else
                {
                    donationAmount = amount;
                }
            }

			int minDonation = getMinimumDonationAmount();

            if(donationAmount < minDonation){
				new AlertDialog.Builder(FundMonthlyDonationActivity.this)
						.setMessage("The minimum amount for our automatic monthly giving program is " + " $" + Integer.toString(minDonation) +
						". If possible, please increase your gift amount. If that's not possible, you can make a one-time contribution of any amount." )
						.setNeutralButton("OK", (dialog, which) -> {}).show();
				return;
            }

            onNext();

        });
		Button aboutButton = (Button) findViewById(R.id.about_card_safety_button);
		if (null != aboutButton) {
			aboutButton.setOnClickListener(view -> onAbout());
		}
		Button skipButton = (Button) findViewById(R.id.skip_button);
		skipButton.setText("No thanks to monthly");
		skipButton.setBackgroundColor(Color.GRAY);


		skipButton.setOnClickListener(v -> onSkip());
		paymentAmountRadioGroup = (RadioGroup) findViewById(R.id.radiogroup_payment_amount);
		
		paymentTypeRadioGroup = (RadioGroup) findViewById(R.id.radiogroup_payment_method);

		amountEditText = (EditText) findViewById(R.id.edit_text_payment_amount);
		paymentAmountRadioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            Log.e("chacked changed", "checked changed");
            updateAmount();
        });

		paymentTypeRadioGroup
		.setOnCheckedChangeListener((group, checkedId) -> updateMethod());
		amountEditText.setOnClickListener(v -> {
            // TODO Auto-generated method stub
            amountEditText.setFocusableInTouchMode(true);
            RadioButton otherRadioButton = (RadioButton) findViewById(R.id.radiobutton_payment_amount_other);
            otherRadioButton.setChecked(true);

        });

		findViewById(R.id.marketing_material_button).setVisibility(View.GONE);
	}

	private int getMinimumDonationAmount() {

		int iRet = 10;
		JSONObject jsonObj = petition.getPetitionJSON();
		JSONArray jsonArr = new JSONArray();
		try {
			jsonArr = jsonObj.getJSONArray("ScriptProperties");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (int i = 0; i < jsonArr.length(); i++) {
			try {
				if (jsonArr.getJSONObject(i).getString("Name").equals("minDonationAmount")) {
						try {
							iRet = Integer.valueOf(jsonArr.getJSONObject(i).getString("Value"));
						} catch(NumberFormatException e) {
							//in the event of bad input data, return the default value to allow folks to continue working.
						}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		return iRet;
	}

	@Override
    public void onResume() {
        super.onResume();
        findViewById(R.id.marketing_material_button).setVisibility(View.GONE);
    }

	public boolean isHrcPayment = false;

	public void getDonationAmountJson()  {
		JSONObject jsonObj = petition.getPetitionJSON();
		JSONArray jsonArr = new JSONArray();
		try {
			jsonArr = jsonObj.getJSONArray("ScriptProperties");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for(int i = 0; i< jsonArr.length() ; i++){
			try {
				if(jsonArr.getJSONObject(i).getString("Name").equals("HRC_DONATION")){
					isHrcPayment = true;
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

}