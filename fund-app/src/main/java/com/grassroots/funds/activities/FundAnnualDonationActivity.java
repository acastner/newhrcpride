package com.grassroots.funds.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.grassroots.funds.R;
import com.grassroots.funds.models.FundQuestions;
import com.grassroots.petition.activities.BaseLoginActivity;
import com.grassroots.petition.models.Cart;
import com.grassroots.petition.models.Question;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FundAnnualDonationActivity extends FundDonationActivity {

	private Button nextButton,backButton = null;
	private EditText amountEditText = null;
	private RadioGroup paymentAmountRadioGroup = null,
			paymentTypeRadioGroup = null;
	private LinearLayout onetimeDonationLayout;
	private CheckBox automaticDonationCheckBox;
	private TextView frequencyView;
	TextView headerTitle = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.annual_donation_activity);

		ImageView headerLogo = (ImageView) findViewById(R.id.header_logo);
		headerLogo.setVisibility(View.VISIBLE);
		
		onetimeDonationLayout = (LinearLayout) findViewById(R.id.onetimeCheckboxLayout);
		automaticDonationCheckBox = (CheckBox) findViewById(R.id.oneTimeDonationCheckBox);
		frequencyView = (TextView) findViewById(R.id.frequencyView);

		headerTitle = (TextView) findViewById(R.id.header_title);
		if (null != headerTitle) {
			headerTitle.setText(getString(R.string.annual_page_header));
		}
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		backButton = (Button) findViewById(R.id.back_button);
		backButton.setOnClickListener(v -> onBackPressed());


		getDonationAmountJson();


		Button skipButton = (Button) findViewById(R.id.skip_button);
		skipButton.setText("No thanks to giving");
		skipButton.setBackgroundColor(Color.GRAY);

		skipButton.setOnClickListener(v -> {
            FundDonationActivity.donation = false;
            startActivity( getNextActivityAfterActivityClass(FundDonationActivity.class) );
        });

		automaticDonationCheckBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            int checkedRadioButtonId = paymentTypeRadioGroup
                    .getCheckedRadioButtonId();
            if(isChecked){

                if (checkedRadioButtonId > 0) {
                    int selectedPaymentType = paymentTypeRadioGroup
                            .getCheckedRadioButtonId();
                    if (selectedPaymentType == R.id.radiobutton_payment_method_card
                            || selectedPaymentType == R.id.radiobutton_payment_method_checking) {
						if(!isHrcPayment) {
							frequencyView.setText("ANNUAL");
						} else {
							frequencyView.setText("ANNUAL RECURRING");
						}
                        headerTitle.setText(getString(R.string.annual_page_header));
                        if(selectedPaymentType == R.id.radiobutton_payment_method_checking){
                            setisAnalog(false);
                            setPaymentMethod(getString(R.string.electronic_check));
                        }

                    }
                }
            }else{
                if (checkedRadioButtonId > 0) {
                    int selectedPaymentType = paymentTypeRadioGroup
                            .getCheckedRadioButtonId();
                    if (selectedPaymentType == R.id.radiobutton_payment_method_card
                            || selectedPaymentType == R.id.radiobutton_payment_method_checking) {
                        frequencyView.setText(RECURRING_ONE_TIME);
                        headerTitle.setText(getString(R.string.onetime_page_header));
                        if(selectedPaymentType == R.id.radiobutton_payment_method_checking){
                            setisAnalog(true);
                            setPaymentMethod("Paper check");

                        }

                    }
                }
            }
        });
		nextButton = (Button) findViewById(R.id.next_button);
		nextButton.setOnClickListener(v -> {

            FundDonationActivity.donation = true;

            RadioButton otherRadioButton = (RadioButton) findViewById(R.id.radiobutton_payment_amount_other);

            float amount = 0;

            if(otherRadioButton.isChecked())
            {

                try
                {
                    amount  = Float.parseFloat(amountEditText.getText()
                            .toString());
                }
                catch (NumberFormatException e)
                {
                    Toast.makeText(FundAnnualDonationActivity.this,
                            "Not a valid donation.", Toast.LENGTH_LONG).show();
                    return;
                }
            }

            int selectedPaymentType = paymentTypeRadioGroup
                    .getCheckedRadioButtonId();
            if ((otherRadioButton.isChecked() && amountEditText.getText() != null)
                    && (amountEditText.getText().toString().length() != 0)
                    && (Float.parseFloat(amountEditText.getText()
                            .toString()) > 0)) {

                //implement this in base(GRU) app.
                if(amount > 999999.99)
                {
                    Toast.makeText(FundAnnualDonationActivity.this,
                            "Donation must be less than $1 million.", Toast.LENGTH_LONG).show();
                    return;
                }
                else
                {
                    donationAmount = amount;
                }
            }

            if (onetimeDonationLayout.getVisibility() == View.VISIBLE) {
                if (automaticDonationCheckBox.isChecked()) {

                    int minDonation = getMinimumDonationAmount();
                    
                    
                    if(donationAmount < minDonation)
                    {
                        new AlertDialog.Builder(FundAnnualDonationActivity.this)
                        .setMessage("The minimum amount for our Automatic Renewal program is $"+Integer.toString(minDonation)+".  " +
                                "If possible, please increase your gift amount.  If that's not possible, " +
                                "you can make a one-time contribution of any amount.")
                        .setNeutralButton("OK", (dialog, which) -> {}).show();
                        return;
                    }

                    setRecurring("ANNUAL");
                    if(selectedPaymentType == R.id.radiobutton_payment_method_checking){
                        setPaymentMethod(getString(R.string.electronic_check));
                    }
                } else {
                    if(selectedPaymentType == R.id.radiobutton_payment_method_checking){
                        setPaymentMethod(getString(R.string.paper_check));
                    }
                    setRecurring(RECURRING_ONE_TIME);
                }
            } else {
                setRecurring(RECURRING_ONE_TIME);
            }
            onNext();

        });

		Button aboutButton = (Button) findViewById(R.id.about_card_safety_button);
		if (null != aboutButton) {
			aboutButton.setOnClickListener(view -> onAbout());
		}

		paymentAmountRadioGroup = (RadioGroup) findViewById(R.id.radiogroup_payment_amount);
		paymentTypeRadioGroup = (RadioGroup) findViewById(R.id.radiogroup_payment_method);
		amountEditText = (EditText) findViewById(R.id.edit_text_payment_amount);

		paymentAmountRadioGroup
		.setOnCheckedChangeListener((group, checkedId) -> updateAmount());

		paymentTypeRadioGroup
		.setOnCheckedChangeListener((group, checkedId) -> {
            updateMethod();
            // show one time donation view
            int checkedRadioButtonId = paymentTypeRadioGroup
                    .getCheckedRadioButtonId();
            if (checkedRadioButtonId > 0) {
                int selectedPaymentType = paymentTypeRadioGroup
                        .getCheckedRadioButtonId();
                if (selectedPaymentType == R.id.radiobutton_payment_method_card
                        || selectedPaymentType == R.id.radiobutton_payment_method_checking) {
                    onetimeDonationLayout
                    .setVisibility(View.VISIBLE);
                    automaticDonationCheckBox.setChecked(true);
					if(!isHrcPayment) {
						frequencyView.setText("ANNUAL");
					} else {
						frequencyView.setText("ANNUAL RECURRING");
					}
                    headerTitle.setText(getString(R.string.annual_page_header));


                } else {
                    onetimeDonationLayout.setVisibility(View.GONE);
                    setRecurring(RECURRING_ONE_TIME);
                    frequencyView.setText(RECURRING_ONE_TIME);
                    headerTitle.setText(getString(R.string.onetime_page_header));

                }

            }
        });

		amountEditText.setOnClickListener(v -> {
            // TODO Auto-generated method stub
            amountEditText.setFocusableInTouchMode(true);
            RadioButton otherRadioButton = (RadioButton) findViewById(R.id.radiobutton_payment_amount_other);
            otherRadioButton.setChecked(true);

        });

        if(isHrcPayment) {
            frequencyView.setText("ANNUAL RECURRING");
        }
	}

    private int getMinimumDonationAmount() {

        int iRet = 25;
        JSONObject jsonObj = petition.getPetitionJSON();
        JSONArray jsonArr = new JSONArray();
        try {
            jsonArr = jsonObj.getJSONArray("ScriptProperties");

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        for (int i = 0; i < jsonArr.length(); i++) {
            try {
                if (jsonArr.getJSONObject(i).getString("Name").equals("minDonationAmountAnnual")) {
                    try {
                        iRet = Integer.valueOf(jsonArr.getJSONObject(i).getString("Value"));
                    } catch(NumberFormatException e) {
                        //in the event of bad input data, return the default value to allow folks to continue working.
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return iRet;
    }

    public boolean isHrcPayment = false;

	public void getDonationAmountJson()  {
		JSONObject jsonObj = petition.getPetitionJSON();
		JSONArray jsonArr = new JSONArray();
		try {
			jsonArr = jsonObj.getJSONArray("ScriptProperties");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for(int i = 0; i< jsonArr.length() ; i++){
			try {
				if(jsonArr.getJSONObject(i).getString("Name").equals("HRC_DONATION")){
					isHrcPayment = true;
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

}
