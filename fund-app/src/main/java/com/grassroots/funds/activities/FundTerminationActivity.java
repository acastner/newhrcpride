package com.grassroots.funds.activities;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.grassroots.funds.R;
import com.grassroots.petition.activities.TerminateQuestionsActivity;
import com.grassroots.petition.views.QuestionListView;

public class FundTerminationActivity extends TerminateQuestionsActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		final QuestionListView questionlistview = (QuestionListView) (findViewById(R.id.question_list));		
		ViewTreeObserver vto = questionlistview.getViewTreeObserver();
		vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
			@Override
			public void onGlobalLayout() {
				questionlistview.getViewTreeObserver()
						.removeGlobalOnLayoutListener(this);
				addRadioButtonListeners();

			}
		});
	}

	private void addRadioButtonListeners() {
//
		TextView headerText = (TextView) findViewById(R.id.header_title);
		headerText.setText(getString(R.string.end_button));
//		
		RadioGroup radioGroup = (RadioGroup) findViewById(R.id.multiple_choice);
		int count = radioGroup.getChildCount();
		for (int i = 0; i < count; i++) {
			View o = radioGroup.getChildAt(i);
			if (o instanceof RadioButton) {
				RadioButton button = (RadioButton) o;
				button.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						QuestionListView questionListView = (QuestionListView) findViewById(R.id.question_list);
						FundTerminationActivity.this.onReview(questionListView
								.getAnswers());
					}
				});
			}
		}
	}
}
