package com.grassroots.funds.activities;

import com.grassroots.petition.activities.BasePetitionActivity;
/**
 * we are not using dob activity anymore
 *
 */
public class FundEmployerActivity extends BasePetitionActivity {
//	protected Question occupationquestion;
//	protected Question employerquestion;
//	protected TextView occupationView;
//	protected TextView employerView;
//	protected Question employercityquestion;
//	protected TextView employercityView;
//	protected Question employerstatequestion;
//	protected TextView employerstateView;
//	
//	private void ConfigureOptionalQuestions()
//	{
//		//OCCUPATION SETTINGS
//		occupationquestion = petition.getQuestionWithVarName(FundQuestions.OCCUPATION);
//		occupationView = (TextView) findViewById(R.id.occupation_edit);
//		if (occupationquestion != null) //only proceed IF this question exists!
//		{
//			occupationView.setVisibility(View.VISIBLE);
//		}
//		else
//		{
//			occupationView.setVisibility(View.GONE);
//		}
//		
//		//EMPLOYER QUESTIONS
//		employerquestion = petition.getQuestionWithVarName(FundQuestions.EMPLOYER);
//		employerView = (TextView) findViewById(R.id.employer_edit);
//		
//		if (employerquestion != null) //only proceed IF this question exists!
//		{
//			employerView.setVisibility(View.VISIBLE);
//		}
//		else
//		{
//			employerView.setVisibility(View.GONE);
//		}
//		
//		//EMPLOYER CITY QUESTIONS
//		employercityquestion = petition.getQuestionWithVarName(FundQuestions.EMPLOYER_CITY);
//		employercityView = (TextView) findViewById(R.id.employer_city_edit);
//		if (employercityquestion != null) //only proceed IF this question exists!
//		{
//			employercityView.setVisibility(View.VISIBLE);
//		}
//		else
//		{
//			employercityView.setVisibility(View.GONE);
//		}
//		
//		//EMPLOYER STATE QUESTIONS
//		employerstatequestion = petition.getQuestionWithVarName(FundQuestions.EMPLOYER_STATE);
//		employerstateView = (TextView) findViewById(R.id.employer_state_edit);
//		if (employerstatequestion != null) //only proceed IF this question exists!
//		{
//			employerstateView.setVisibility(View.VISIBLE);
//		}
//		else
//		{
//			employerstateView.setVisibility(View.GONE);
//		}
//	}
//	
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.employment);
//		ConfigureOptionalQuestions();
//		Button nextButton = (Button) findViewById(R.id.next_button);
//		nextButton.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				
//				Question DOBQuestion = petition.getQuestionWithVarName(FundQuestions.DATE_OF_BIRTH);
//				TextView DOBNameView = (TextView) findViewById(R.id.date_of_birth);
//				if(DOBNameView.getText().toString().length()>0){
//					if (isValidDate(DOBNameView.getText().toString(),"MMddyyyy"))
//					petitionAnswers.overwriteAnswer(new SubjectAnswer(DOBQuestion.getId(), DOBNameView.getText().toString()));					
//					else
//					{
//						notifyUser("Invalid date format. Date entered must be 2 digit month followed by a 2 digit day followed by 4 digit year.");
//						DOBNameView.setText(null);
//						DOBNameView.requestFocus();
//						return;
//					}
//				}
//				if (occupationquestion != null) //only proceed with validating and setting the answer IF...this question exists!
//				{
//					if(occupationquestion.isRequired() && occupationView.getText().toString().length()==0){
//						Toast.makeText(FundEmployerActivity.this, "Please enter occupation", Toast.LENGTH_LONG).show();
//						occupationView.requestFocus();
//						return;
//					}else{
//					    petitionAnswers.overwriteAnswer(new SubjectAnswer(occupationquestion.getId(), occupationView.getText().toString()));
//					}
//				}
//				
//				if (employerquestion != null) //only proceed with validating and setting the answer IF...this question exists!
//					{
//					if(employerquestion.isRequired() && employerView.getText().toString().length()==0){
//						Toast.makeText(FundEmployerActivity.this, "Please enter employer name", Toast.LENGTH_LONG).show();
//						employerView.requestFocus();
//						return;
//					}else{
//						petitionAnswers.overwriteAnswer(new SubjectAnswer(employerquestion.getId(), employerView.getText().toString()));
//					}
//				}
//				
//				if (employercityquestion != null) //only proceed with validating and setting the answer IF...this question exists!
//				{
//					if(employercityquestion.isRequired() && employercityView.getText().toString().length()==0){
//						Toast.makeText(FundEmployerActivity.this, "Please enter employer city", Toast.LENGTH_LONG).show();
//						employercityView.requestFocus();
//						return;
//					}else{
//						petitionAnswers.overwriteAnswer(new SubjectAnswer(employercityquestion.getId(), employercityView.getText().toString()));
//					}
//				}
//
//				if(employerstatequestion != null)
//				{
//					if(employerstatequestion.isRequired() && employerstateView.getText().toString().length()==0){
//						Toast.makeText(FundEmployerActivity.this, "Please enter employer state", Toast.LENGTH_LONG).show();
//						employerstateView.requestFocus();
//						return;
//					}else{
//						petitionAnswers.overwriteAnswer(new SubjectAnswer(employerstatequestion.getId(), employerstateView.getText().toString()));
//					}
//				}
//				
//				Question partnerSalutationQuestion = petition.getQuestionWithVarName(FundQuestions.PARTNER_SALUTATION);
//				TextView partnerSalutationView = (TextView) findViewById(R.id.partner_salutation);
//				if(partnerSalutationView.getText().toString().length()>=0){
//					petitionAnswers.overwriteAnswer(new SubjectAnswer(partnerSalutationQuestion.getId(), partnerSalutationView.getText().toString()));					
//				}
//				
//				Question partnerFirstNameQuestion = petition.getQuestionWithVarName(FundQuestions.PARTNER_FIRST_NAME);
//				TextView partnerFirstNameView = (TextView) findViewById(R.id.partner_first_name);
//				if(partnerFirstNameView.getText().toString().length()>=0){
//					petitionAnswers.overwriteAnswer(new SubjectAnswer(partnerFirstNameQuestion.getId(), partnerFirstNameView.getText().toString()));					
//				}
//				
//				Question partnerLastNameQuestion = petition.getQuestionWithVarName(FundQuestions.PARTNER_LAST_NAME);
//				TextView partnerLastNameView = (TextView) findViewById(R.id.partner_last_name);
//				if(partnerLastNameView.getText().toString().length()>0){
//					petitionAnswers.overwriteAnswer(new SubjectAnswer(partnerLastNameQuestion.getId(), partnerLastNameView.getText().toString()));					
//				}
//				
//				Question partnerDOBQuestion = petition.getQuestionWithVarName(FundQuestions.PARTNER_DATE_OF_BIRTH);
//				TextView partnerDOBNameView = (TextView) findViewById(R.id.partner_date_of_birth);
//				if(partnerDOBNameView.getText().toString().length()>0){
//					if (isValidDate(partnerDOBNameView.getText().toString(),"MMddyyyy"))
//					petitionAnswers.overwriteAnswer(new SubjectAnswer(partnerDOBQuestion.getId(), partnerDOBNameView.getText().toString()));					
//					else
//					{
//						notifyUser("Invalid date format. Date entered must be 2 digit month followed by a 2 digit day followed by 4 digit year.");
//						partnerDOBNameView.setText(null);
//						partnerDOBNameView.requestFocus();
//						return;
//					}
//				}				
//				
//				startActivity(getNextActivity());
//			}
//		});
//		
//	}
//	
//	private boolean isValidDate(String strdate,String strFormat)
//	{
//		try
//		{
//			SimpleDateFormat sdf = new SimpleDateFormat(strFormat);
//			sdf.setLenient(false);
//			Date date = sdf.parse(strdate,new ParsePosition(0));
//			if (date == null)
//			{
//				notifyUser("Could not parse date entered.");
//				return false;
//			}
//			SimpleDateFormat dtYear = new SimpleDateFormat("yyyy");
//			int year = Integer.parseInt(dtYear.format(date));
//			if (year<1900 || year > 2100)
//			{
//				notifyUser("Invalid year "+year + " year cannot be greater than 2100 or less than 1900");
//				return false;
//			}
//			return true;
//		}		
//		catch(IllegalArgumentException e)
//		{
//			return false;
//		}
//	}
//
//	public static boolean shouldProceedWithPetition (Petition petition)
//    {
//        return true;
//    }
	
	
}
