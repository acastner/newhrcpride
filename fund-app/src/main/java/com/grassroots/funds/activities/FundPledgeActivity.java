package com.grassroots.funds.activities;

import java.util.List;

import org.apache.log4j.Logger;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Debug;
import android.os.Debug.MemoryInfo;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.grassroots.funds.R;
import com.grassroots.funds.models.FundQuestions;
import com.grassroots.petition.GlobalData;
import com.grassroots.petition.activities.BasePetitionActivity;
import com.grassroots.petition.models.Petition;
import com.grassroots.petition.models.Question;
import com.grassroots.petition.models.QuestionAnswer;
import com.grassroots.petition.models.SubjectAnswer;
import com.grassroots.petition.utils.Tracker;
import com.grassroots.petition.views.QuestionView;
import com.grassroots.petition.views.QuestionsView;

public class FundPledgeActivity extends BasePetitionActivity {
	private static final String TAG = FundPledgeActivity.class.getName();
	private static final Logger LOGGER = Logger.getLogger(TAG);

	private QuestionsView view;
	private QuestionView questionview;
	private Question question;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ActivityManager activityManager = (ActivityManager) this.getBaseContext().getSystemService("activity");
		List<RunningAppProcessInfo> runningAppProcesses = activityManager.getRunningAppProcesses();
		// for(RunningAppProcessInfo runningProcessInfo : runningAppProcesses){
		// Log.e("processid", "process id :"+runningProcessInfo.pid);
		// Log.e("processName : ", "process name :
		// "+runningProcessInfo.processName);
		//
		// }

		int[] pids = new int[runningAppProcesses.size()];
		for (int i = 0; i < runningAppProcesses.size(); i++) {
			ActivityManager.RunningAppProcessInfo info = runningAppProcesses.get(i);
			pids[i] = info.pid;
		}

		Debug.MemoryInfo[] procsMemInfo = activityManager.getProcessMemoryInfo(pids);
		for (int i = 0; i < procsMemInfo.length; i++) {
			MemoryInfo procMem = procsMemInfo[i];
			Tracker.appendLog("Process Name : " + runningAppProcesses.get(i).processName);
			Tracker.appendLog("dalvikPrivateDirty : " + procMem.dalvikPrivateDirty);
			Tracker.appendLog("dalvikPss : " + procMem.dalvikPss);
			Tracker.appendLog("getTotalPrivateDirty : " + procMem.getTotalPrivateDirty());
			Tracker.appendLog("getTotalPss : " + procMem.getTotalPss());
		}

		question = petition.getQuestionWithVarName(FundQuestions.PLEDGE);
		view = (QuestionsView) View.inflate(this, R.layout.questions, null);
		view.setPetitionAnswers(petitionAnswers);
		view.setQuestion(question);
		setContentView(view);

		questionview = (QuestionView) (findViewById(R.id.question));
		questionview.setQuestion(question);
		hideMarketingIfHasNoMaterials();
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		addRadioButtonListeners();
		configureBackButton();
		configureNextButton();
		configureMarketingMaterialsButton();
		// ABC 9/4/14 We're getting multiple memory related crashes that all
		// happen in this section of the app. This is brute force, but I dont
		// have the depth to do a full memory teardown. Putting double gc
		// collection hints in.
		LOGGER.debug("Hinting to gc to free memory. current free mem = " + Runtime.getRuntime().freeMemory());
		Runtime.getRuntime().gc();
	}

	private void configureMarketingMaterialsButton() {
//		findViewById(R.id.marketing_material_button).setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View view) {
//				onMarketing();
//			}
//		});
	}

	/**
	 * Configures next button.
	 */
	private void configureNextButton() {

		findViewById(R.id.next_button).setVisibility(View.GONE);

		findViewById(R.id.next_button).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				SubjectAnswer answer = questionview.getAnswer();

				if (question.isRequired() && !answer.hasAnswer()) {
					Toast.makeText(FundPledgeActivity.this, "This question is required", Toast.LENGTH_LONG).show();
					return;
				} else {
					petitionAnswers.overwriteAnswer(answer);
				}

				// 8/26/14 ABC this next bit is custom code just for the fund,
				// it looks to see if theres a match on the branch to the
				// healthcare question and if there is, starts the activity,
				// else it goes to conclusion.
				QuestionAnswer questionAnswer = question.getAnswerWithId(answer.getAnswerId());
				if (questionAnswer.hasBranch()) {
					if (questionAnswer.getBranch() == petition
							.getQuestionWithVarName(FundQuestions.FLAG_HEALTHCAREPROVIDER).getId()) {
						startActivity(FundHealthCareActivity.class);
					} else {
						startActivity(FundConclusionActivity.class);
					}
				} else {
					startActivity(FundConclusionActivity.class);
				}

			}
		});
	}

	/**
	 * Configures back button.
	 */
	private void configureBackButton() {
		findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
	}

	private void addRadioButtonListeners() {
		WebView webview = getGlobalData().getWebview();

		LinearLayout webViewParent = (LinearLayout) findViewById(R.id.webviewParent);
		webview = ((GlobalData) (GlobalData.getContext())).getWebview();
		if (webview != null) {
			webview.stopLoading();
			webview.clearView();
			webview.clearHistory();
			webview.clearCache(true);

			webview.loadUrl("about:blank");

			webview.setVisibility(View.GONE);
			if (webview.getParent() != null) {
				((ViewGroup) webview.getParent()).removeAllViews();
			}

		}

		RadioGroup radioGroup = (RadioGroup) findViewById(R.id.multiple_choice);
		int count = radioGroup.getChildCount();
		for (int i = 0; i < count; i++) {
			View o = radioGroup.getChildAt(i);
			if (o instanceof RadioButton) {
				RadioButton button = (RadioButton) o;
				button.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						FundPledgeActivity.this.onNext();
					}
				});
			}
		}
	}

	protected void onNext() {
		SubjectAnswer answer = questionview.getAnswer();

		if (question.isRequired() && !answer.hasAnswer()) {
			Toast.makeText(FundPledgeActivity.this, "This question is required", Toast.LENGTH_LONG).show();
			return;
		} else {
			petitionAnswers.overwriteAnswer(answer);
		}

		String renewal = getSubject().getOtherFieldsMapping().get("CARDTYPE");

		Question question = petition.getQuestionWithVarName(FundQuestions.GRASSROOTS);

		if (question != null && !FundDonationActivity.donation && (renewal == null
				|| (!renewal.equals("Renewal") && !renewal.equals("AllStar")
				&& !renewal.equals("Special Appeal") && !renewal.equals("Sustainer Upgrade")))) {
			if (getSubject().getEmail().isEmpty()) {
				startActivity(getNextActivity());
			} else {
				Intent intent = new Intent(FundPledgeActivity.this, FundGrassrootsActivity.class);
				startActivity(intent);
			}
			

		} else {
			startActivity(getNextActivity());
		}

	}

	public static boolean shouldProceedWithPetition(Petition petition) {
		return true;
	}
}
