package com.grassroots.funds.activities;

import com.grassroots.funds.models.FundQuestions;
import com.grassroots.petition.R;
import com.grassroots.petition.activities.BasePetitionActivity;
import com.grassroots.petition.models.Question;
import com.grassroots.petition.models.SubjectAnswer;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class PACMonthlyActivity extends BasePetitionActivity {
	private Question question;
	private Button nextButton, backButton, noThanksButton;

	private Button button50, button25, button10;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pacmonthly);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		question = petition.getQuestionWithVarName(Question.PACMONTHLY);
		//TextView pacmonthlyTitleView = (TextView) findViewById(R.id.pacabove25title);
		//pacmonthlyTitleView.setText(question.getText());

		button50 = (Button) findViewById(R.id.percent_50_button);
		button50.setVisibility(View.GONE);
		button25 = (Button) findViewById(R.id.percent_25_button);
		button25.setVisibility(View.GONE);
		button10 = (Button) findViewById(R.id.percent_10_button);
		nextButton = (Button) findViewById(R.id.next_button);
		backButton = (Button) findViewById(R.id.back_button);
		noThanksButton = (Button) findViewById(R.id.nothanks_button);

		button50.setOnClickListener(view -> savePercentQuestionAnswer("50%"));
		button25.setOnClickListener(view -> savePercentQuestionAnswer("25%"));
		button10.setOnClickListener(view -> savePercentQuestionAnswer("10%"));
		backButton.setOnClickListener(v -> onBackPressed());
		
		noThanksButton.setOnClickListener(v -> {
            String percentage = "0%";
            SubjectAnswer subjectAnswer = new SubjectAnswer(question.getId(), percentage);
            petitionAnswers.addAnswer(subjectAnswer);
            startActivity( getNextActivityAfterActivityClass(FundDonationActivity.class) );
        });
	}

	@Override
	public void onBackPressed() {
		try {
			petitionAnswers.removeAnswer(petition.getQuestionWithVarName(Question.PACMONTHLY));
		} catch(Exception ex) {

		}
		super.onBackPressed();
	}


	private void savePercentQuestionAnswer(String percentage) {
		SubjectAnswer subjectAnswer = new SubjectAnswer(question.getId(), percentage);
		petitionAnswers.addAnswer(subjectAnswer);
		Question occupationQuestion = petition.getQuestionWithVarName(FundQuestions.OCCUPATION);
		Question employerQuestion = petition.getQuestionWithVarName(FundQuestions.EMPLOYER);
		Question disclaimerQuestion = petition.getQuestionWithVarName(FundQuestions.PAC_DISCLAIMER);

		if(disclaimerQuestion != null) {
			startActivity(PACDisclaimerActivity.class);
		} else if(occupationQuestion != null && employerQuestion != null) {
			startActivity( FundOccupationEmployerActivity.class );
		} else {
			startActivity( getNextActivityAfterActivityClass(FundDonationActivity.class) );
		}
	}
	
	
}
