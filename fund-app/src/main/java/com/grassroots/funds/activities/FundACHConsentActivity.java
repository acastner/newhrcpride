package com.grassroots.funds.activities;


import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.grassroots.funds.R;
import com.grassroots.funds.models.FundQuestions;
import com.grassroots.petition.activities.BasePetitionActivity;
import com.grassroots.petition.models.ACHData;
import com.grassroots.petition.models.CardData;
import com.grassroots.petition.models.Question;
import com.grassroots.petition.models.Signature;
import com.grassroots.petition.views.SignatureCanvas;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class FundACHConsentActivity extends BasePetitionActivity {

	private Button nextButton,seeDisclosureButton;
	private Question question;
	private TextView nameView,amtView,typeView,frequencyView,donInfoLabelView,disclaimerView;
	private static final String TAG = FundACHConsentActivity.class.getName();
	private static final Logger LOGGER = Logger.getLogger(TAG);
	SignatureCanvas sigCanvassView;
	float donAmt ;
	String recurring;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		setContentView(R.layout.ach_consent_screen);

		question = petition.getQuestionWithVarName(FundQuestions.SIGNATURE_CONSENT);

		nameView = (TextView) findViewById(R.id.nameView);
		amtView = (TextView) findViewById(R.id.amtView);
		typeView = (TextView) findViewById(R.id.typeView);
		frequencyView = (TextView) findViewById(R.id.frequencyView);
		donInfoLabelView = (TextView) findViewById(R.id.donInfoLabel);
		seeDisclosureButton = (Button) findViewById(R.id.disclaimer_text_button);
		sigCanvassView = (SignatureCanvas) findViewById(R.id.signature_canvas);
		disclaimerView = (TextView) findViewById(R.id.disclaimer_text);

		getDonationAmountJson();
		setViews();

		FundDonationActivity.donation = true;
	}

	private boolean check = false;
	private boolean card = false;

	boolean isHrcPayment = false;

	public void getDonationAmountJson()  {
		JSONObject jsonObj = petition.getPetitionJSON();
		JSONArray jsonArr = new JSONArray();
		try {
			jsonArr = jsonObj.getJSONArray("ScriptProperties");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for(int i = 0; i< jsonArr.length() ; i++){
			try {
				if(jsonArr.getJSONObject(i).getString("Name").equals("HRC_DONATION")){
					isHrcPayment = true;
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	private void setViews() {
		Button clear = (Button) findViewById(R.id.clear_signature_button);
		String cost = getCart().getItems().get(0).getCost();
		SharedPreferences sharedPref = getSharedPreferences(
				getString(R.string.preference_file_key), Context.MODE_PRIVATE);
		recurring = sharedPref.getString("recurring", null);
		String disclaimerText =question.getText();
		nameView.setText(getSubject().getFirstName() + " " + getSubject().getLastName());
		donAmt = Float.parseFloat(cost);
		amtView.setText("$" +String.format("%.2f", donAmt) );
		//donInfoLabelView.setText(question.getName());

		if(recurring.equalsIgnoreCase("Monthly")){
			frequencyView.setText("MONTHLY");

			if(isHrcPayment) {
				frequencyView.setText("MONTHLY RECURRING");
			}
		}else if(recurring.equalsIgnoreCase("One-time")){
			frequencyView.setText("ONE-TIME");
		}else if(recurring.equalsIgnoreCase("Annual")){
			frequencyView.setText("ANNUAL - Automatic Renewal  Each Year");

			if(isHrcPayment) {
				frequencyView.setText("ANNUAL RECURRING");
			}
		}

		//disclaimerView.setText(disclaimerText);


		String lastdigits = null;

		if(getCart().getPaymentDataType().equals(ACHData.class.getName()))
		{
			typeView.setText("Electronic Check");
			disclaimerView.setText(R.string.monthly_consent_statement);
			check = true;
			card = false;

		}else if(getCart().getPaymentDataType().equals(CardData.class.getName()))
		{
			typeView.setText("Credit Card");
			disclaimerView.setText(getString(R.string.monthly_consent_statement));
			check = false;
			card = true;
		}else
		{
			if(lastdigits == null || lastdigits.trim().length() == 0)
			{
				lastdigits = ".";
			}			
		}



		clear.setVisibility(View.GONE);

		clear.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				sigCanvassView.clearSignature();

			}
		});

		Log.e("cost", "cost : " + cost);
		nextButton = (Button) findViewById(R.id.next_button);
		nextButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				question = petition.getQuestionWithVarName(FundQuestions.SIGNATURE_CONSENT);
				petition.setSignature(true);
				if(!sigCanvassView.hasSignatureData()){
					notifyUser("Please provide your signature");
					return;
				}

				int iRetryCount =3;
				boolean bCaptureStatus = false;
				for (int i = 0; i < iRetryCount; i++) {
					bCaptureStatus=GetScreenCapture();
					if (bCaptureStatus)
					{
						notifyUser("Successfully captured signature.");
						break;
					}
				}

				if (bCaptureStatus==false)
				{
					notifyUser("Unable to capture signature, please retry.");
					Button clear = (Button) findViewById(R.id.clear_signature_button);
					clear.performClick();
					return;
				}
				
				

				Question occupationQuestion = petition.getQuestionWithVarName(FundQuestions.OCCUPATION);
				Question employerQuestion = petition.getQuestionWithVarName(FundQuestions.EMPLOYER);
				
//				if(occupationQuestion != null && employerQuestion != null)
//				{
//					startActivity( FundOccupationEmployerActivity.class );
//				}
				if(recurring.equalsIgnoreCase("One-time") && check) {
					startActivity( getNextActivityAfterActivityClass(FundDonationActivity.class) );
				} else if(recurring.equalsIgnoreCase("One-time") && card && donAmt > 25){
					Question q = petition.getQuestionWithVarName(Question.PACABOVE25);
					//Only show PACAbove25 if question exists / is active
					if(q != null) {
						startActivity( PACAbove25Activty.class );
					} else {
						startActivity( getNextActivityAfterActivityClass(FundDonationActivity.class) );
					}
				}else if( recurring.equalsIgnoreCase("Annual") && donAmt > 25){
					Question q = petition.getQuestionWithVarName(Question.PACABOVE25);

					//Only show PACAbove25 if question exists / is active
					if(q != null) {
						startActivity( PACAbove25Activty.class );
					} else {
						startActivity( getNextActivityAfterActivityClass(FundDonationActivity.class) );
					}

				}else if( recurring.equalsIgnoreCase("Monthly")){
					Question q = petition.getQuestionWithVarName(Question.PACMONTHLY);

					//Only show PACMONTHLY if question exists / is active
					if(q != null) {
						startActivity(PACMonthlyActivity.class);
					} else {
						startActivity( getNextActivityAfterActivityClass(FundDonationActivity.class) );
					}
				}
				else
				{
					startActivity( getNextActivityAfterActivityClass(FundDonationActivity.class) );
				} 
			}
		});

		seeDisclosureButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent = new Intent(FundACHConsentActivity.this, FundDisclosueLanguageActivity.class);
				startActivity(intent);
			}
		});
		
		 Button backButton = (Button) findViewById(R.id.back_button);
	        backButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
				    onBackPressed();
					
				}
			});

	}

	/*
	@Override
	public void onBackPressed() {
		super.onBackPressed();

	}
	*/

	private boolean GetScreenCapture()
	{
		boolean bReturn = false;
		try{

			View view = FundACHConsentActivity.this.getWindow().getDecorView();
			View rootview = view.findViewById(R.id.rootview);

			Bitmap bitmap = Bitmap.createBitmap(rootview.getMeasuredWidth(), 
					rootview.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
			Canvas bitmapHolder = new Canvas(bitmap); 
			rootview.draw(bitmapHolder);
			Bitmap bm = bitmap.copy(Bitmap.Config.ARGB_8888, false);
			File file = new File(Environment.getExternalStorageDirectory()+"/consentform.png");

			//try catch below compresses the file. if it fails, we'll just have a larger file. not a big deal. 
			try 
			{
				file.createNewFile();
				FileOutputStream ostream = new FileOutputStream(file);
				bitmap.compress(CompressFormat.PNG, 100, ostream);
				if (ostream!=null)
					ostream.close();

				ostream=null;
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}

			
			bm = Bitmap.createScaledBitmap(bm, (int)(bm.getWidth()), (int)(bm.getHeight()), false);
			//bm = Bitmap.createScaledBitmap(bm, 400, 300, false);
			disclaimerView.setDrawingCacheEnabled(true);
			Signature signatureWithDisclaimer = new Signature(combineImages(disclaimerView.getDrawingCache(), bitmap));
			Signature signatureWithoutDisclaimer = new Signature(bitmap);
			if(getSignatureFormat()) {
				petitionAnswers.setSignature(signatureWithDisclaimer);
			}
			else	{
				petitionAnswers.setSignature(signatureWithoutDisclaimer);
			}
			rootview.destroyDrawingCache();

			if (file.exists())
				file.delete();

			bm = null;
			bitmap=null;
			bReturn = true;
		}
		catch(Exception e)
		{
			bReturn = false;
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			LOGGER.error(errors.toString());
		}
		//ABC 9/4/14 We're getting multiple memory related crashes that all happen in this section of the app. This is brute force, but I dont have the depth to do a full memory teardown. Putting double gc collection hints in.
		LOGGER.debug("Hinting to gc to free memory. current free mem = "+Runtime.getRuntime().freeMemory());

		Runtime.getRuntime().gc();

		return bReturn;
	}

	public boolean getSignatureFormat()	{
		boolean signatureFormat = false;
		JSONObject jsonObj = petition.getPetitionJSON();
		JSONArray jsonArr = new JSONArray();
		try {
			jsonArr = jsonObj.getJSONArray("ScriptProperties");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (int i = 0; i < jsonArr.length(); i++) {

			try {
				if (jsonArr.getJSONObject(i).getString("Name").equals("IncludeDisclaimerInSignatureCapture")) {
					try {
						String signatureFormatString = jsonArr.getJSONObject(i).getString("Value");
						if (signatureFormatString.equals("true"))	{
							signatureFormat = true;
						}
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}

		return signatureFormat;
	}

	public Bitmap combineImages(Bitmap c, Bitmap s){
		Bitmap cs = null;
		int width, height = 0;

		if(c.getWidth() > s.getWidth()) {
			width = c.getWidth();
			height = c.getHeight() + s.getHeight();
		} else {
			width = s.getWidth() ;
			height = c.getHeight() + s.getHeight();
		}

		cs = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

		Canvas comboImage = new Canvas(cs);
		comboImage.drawColor(Color.WHITE);
		comboImage.drawBitmap(c, 0f, 0f, null);
		comboImage.drawBitmap(s, 0f, c.getHeight(), null);

		return cs;
	}

}
