package com.grassroots.funds.activities;

import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.grassroots.petition.R;
import com.grassroots.petition.activities.BaseACHDataActivity;
import com.grassroots.petition.models.ACHData;
import com.grassroots.petition.models.Cart;
import com.grassroots.petition.models.ProductOrder;

public class FundACHActivity extends BaseACHDataActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		EditText accountNumberField = (EditText) findViewById(R.id.ach_data_account_number_editText);
		accountNumberField
				.setOnEditorActionListener(new EditText.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {
						if (actionId == EditorInfo.IME_ACTION_SEARCH
								|| actionId == EditorInfo.IME_ACTION_DONE
								|| event.getAction() == KeyEvent.ACTION_DOWN
								&& event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
							onNext();
							return true;
						}
						return false;
					}
				});
	}

	private int MAXROUTERCODE = 15; // In the db, this can only be 15 chars, not
									// sure at this point if thats an ACH
									// limitation or what. --ABC 7/31/14

	@Override
	public void onNext() {
		// validate fields
		String routingNumber = routingNumberEditText.getText().toString();
		if (9 != routingNumber.length()) {
			notifyUser(getString(R.string.ACH_DATA_INVALID_ROUTING_NUMBER_MESSAGE));
			return;
		}

		String accountNumber = accountNumberEditText.getText().toString();
		if (0 == accountNumber.length()) {
			notifyUser(getString(R.string.ACH_DATA_INVALID_ACCOUNT_NUMBER_MESSAGE));
			return;
		}

		try {
			// FIXME: wrong way to check if all chars are numeric.
			// Integer routingNumberInteger = Integer.parseInt(routingNumber);
		} catch (NumberFormatException e) {
			notifyUser(getString(R.string.INVALID_ROUTING_VALUE_MESSAGE));
			return;
		}

		Cart cart = getCart();
		if (null == cart || 0 == cart.getItems().size()) {
			notifyUser(getString(R.string.NO_PRODUCT_WAS_SELECTED_MESSAGE));
			return;
		}

		cart.setPaymentData(new ACHData(routingNumber, accountNumber,
				"Checking"));

		String renewal = getSubject().getOtherFieldsMapping().get("CARDTYPE");
		if (renewal != null && (renewal.equalsIgnoreCase("Sustainer Upgrade") || renewal.equalsIgnoreCase("AR Upgrade"))) {
			cart.setNoProcessing(true);
		}

		List<ProductOrder> productorder = getCart().getItems();
		String sku = productorder.get(0).getSKU();
		Log.e("sku", "sku : " + sku);
		/*startActivity(new Intent(FundACHActivity.this,
				FundACHConsentActivity.class));*/
		
		if(FundSubmitActivity.paymentRedoStatus == 1)
		{
			FundSubmitActivity.paymentRedoStatus = 0;
			startActivity(new Intent(FundACHActivity.this,FundSubmitActivity.class)
			.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
		}
		else
		{
			startActivity(new Intent(FundACHActivity.this,FundACHConsentActivity.class));
		}

	}
}
