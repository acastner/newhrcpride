package com.grassroots.funds.activities;

import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;

import com.grassroots.api.models.ExecuteSaleData;
import com.grassroots.api.models.ExecuteSaleResponse;
import com.grassroots.api.models.ExecuteSaleResponse.ExecuteSaleResponseMessage;
import com.grassroots.api.models.Transaction;
import com.grassroots.funds.R;
import com.grassroots.libs.util.DeviceInfoUtils;
import com.grassroots.libs.util.DeviceStateUtils;
import com.grassroots.libs.util.PaymentUtils;
import com.grassroots.libs.util.TransactionStatusSerializer;
import com.grassroots.petition.GlobalData;
import com.grassroots.petition.activities.BaseSubmitActivity;
import com.grassroots.petition.activities.WalklistDetailsActivity;
import com.grassroots.petition.db.helper.WalklistDataSource;
import com.grassroots.petition.models.ACHData;
import com.grassroots.petition.models.CardData;
import com.grassroots.petition.models.Cart;
import com.grassroots.petition.models.KnockStatus;
import com.grassroots.petition.models.Question;
import com.grassroots.petition.models.Subject;
import com.grassroots.petition.models.SubjectAnswer;
import com.grassroots.petition.services.GrassrootsRestClient;
import com.grassroots.petition.utils.Dates;
import com.grassroots.petition.utils.LocationProcessor;
import com.grassroots.petition.utils.Strings;
import com.grassroots.petition.utils.Tracker;
import com.grassroots.petition.views.ConfirmCancelDialogBuilder;
import com.grassroots.petition.views.PictureQuestionView;
import com.grassroots.petition.views.QuestionView;
import com.grassroots.petition.views.SubmitView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * TODO: Allow users to edit all of their responses. (DEL-167) TODO: Make this
 * screen a lot prettier.
 */
public class FundSubmitActivity extends BaseSubmitActivity {

    private static final String TAG = FundSubmitActivity.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);
    private static final int PHOTO_ACTION_CODE = 8;
    private SubmitView view;
    private QuestionView missingQuestionView;
    private String photoPath;
    protected Subject subject;
    ProgressDialog pd = null;

    public static final String WALKLIST_EXTRA_PARAMETER_LASTNAME = "SELECTED_LASTNAME_EXTRA";
    public static final String WALKLIST_EXTRA_PARAMETER_SUBJECT = "SELECTED_SUBJECT_EXTRA";

    private List<Transaction> transactionList;
    private Transaction currentTransaction;

    final int CREDIT_ERROR = 2, ACH_ERROR = 1, SERVER_ERROR = 3, SUBMIT_OK = 0, SUBMIT_PENDING = 4;
    int submitResult = SUBMIT_OK; // determines if submission should go through. 0 = yes, 1 or 2 = no
    String resultMessage, errorMsg, processPaymentTitle, processPaymentMsg;
    public static int paymentRedoStatus = 0;

    public static final int PAYMENT_METHOD_TYPE_CC = 1;
    public static final int PAYMENT_METHOD_TYPE_ACH = 2;

    @Override
    public void onResume() {
        super.onResume();
        //processPayment(false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initTransactionList();
        subject = getSubject();
        view = (SubmitView) View.inflate(this, R.layout.submit, null);
        view.setViewListener(viewListener);
        view.setSubject(subject, petition);
        view.setPetitionAnswers(petitionAnswers, petition);
        setContentView(view);
    }


    private DialogInterface.OnClickListener getNegativeClickListener() {
        return (dialogInterface, i) -> {
        };
    }

    private DialogInterface.OnClickListener getRegularQuestionListener() {
        return (dialogInterface, i) -> {
            SubjectAnswer newAnswer = missingQuestionView.getAnswer();
            petitionAnswers.overwriteAnswer(newAnswer);
            view.setPetitionAnswers(petitionAnswers, petition);
        };
    }

    private PictureQuestionView.ViewListener getPhotoViewListener() {
        return () -> {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            try {
                File photoFile = createImageFile();
                photoPath = photoFile.getAbsolutePath();
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
            } catch (IOException e) {
                LOGGER.error("Unable to get output file", e);
            }
            startActivityForResult(takePictureIntent, PHOTO_ACTION_CODE);
        };
    }

    // TODO: The dialog has lots of space above and below the image.
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode != PHOTO_ACTION_CODE || resultCode != RESULT_OK) {
            return;
        }

        // Scale and display image
        if (Strings.isNotEmpty(photoPath)) {
            Bitmap photo = readScaledPhoto(photoPath);
            missingQuestionView.setPhoto(photo, photoPath);
        }
    }


    private boolean hasElectronicPaymentProcessing() {
        return getCart().getPaymentDataType() != null && petition.getPaymentProcessorId() == 2;
    }

    private boolean hasPayment() {
        Cart cart = petitionAnswers.getCart();
        if (cart != null && (cart.isCashPayment() || cart.isPaperCheckPayment() || cart.getPaymentDataType() != null)) {
            return true;
        }
        return false;
    }

    private void setSurveyEnd() {
        Calendar cl = Calendar.getInstance();
        int month = cl.get(Calendar.MONTH) + 1;
        int day = cl.get(Calendar.DAY_OF_MONTH);
        int year = cl.get(Calendar.YEAR);
        int hour = cl.get(Calendar.HOUR_OF_DAY);
        int min = cl.get(Calendar.MINUTE);
        int sec = cl.get(Calendar.SECOND);
        petitionAnswers.setSurveyEnd(month + "-" + day + "-" + year + ", " + hour + ":" + min + ":" + sec);
    }


    private void processElectronicPaymentFirstTime() {
        LOGGER.debug("PAYMENT " + getCart().getPaymentDataType());
        String cost = NumberFormat.getCurrencyInstance()
                .format(Double.parseDouble(getCart().getItems().get(0).getCost())).toString();

        processPaymentTitle = "Process Payment?";
        processPaymentMsg = "You will be charged " + cost;

        new AlertDialog.Builder(this).setTitle(processPaymentTitle).setMessage(processPaymentMsg)
                .setPositiveButton(android.R.string.yes, (dialog, which) -> {
                    setSurveyEnd();
                    pd = ProgressDialog.show(FundSubmitActivity.this, "Please wait", "Processing Payment ...");


                    SaveAnswersTask saveAnswersTask = new SaveAnswersTask(false);
                    saveAnswersTask.execute();
                }).setNegativeButton(android.R.string.no, (dialog, which) -> {
        })
                .setIcon(android.R.drawable.ic_dialog_alert).show();
    }

    protected void onNext() {

        if (!hasPayment()) {
            setSurveyEnd();
            pd = ProgressDialog.show(FundSubmitActivity.this, "Please wait", "Submiting survey ...");
            SaveSurveyTask task = new SaveSurveyTask();
            task.execute();
        }

        if (hasElectronicPaymentProcessing()) {
            processElectronicPaymentFirstTime();
        } else if (hasPayment()) {
            setSurveyEnd();
            SaveAnswersTask saveAnswersTask = new SaveAnswersTask(false);
            saveAnswersTask.execute();
        }
    }

    @Override
    public void onReload() {
        super.onReload();
        subject = getSubject();
        view.setSubject(subject, petition);
        view.setPetitionAnswers(petitionAnswers, petition);
        Button b = (Button) findViewById(R.id.submit_button);
        b.setVisibility(View.VISIBLE);
    }

    private void setCurrentTransactionStatus() {
        Cart cart = petitionAnswers.getCart();
        cart.setMobileId(saleTokenPreference.get());
        petitionAnswers.setCart(cart);

        currentTransaction = new Transaction();
        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.preference_file_key),
                Context.MODE_PRIVATE);
        String recurring = sharedPref.getString("recurring", null);
        String type = null;
        if (petitionAnswers.getCart().isCashPayment()) {
            type = "Cash";
            currentTransaction.setStatus(Transaction.TRANSACTION_STATUS_PROCESSED_MOBILE);
        } else if (petitionAnswers.getCart().isPaperCheckPayment()) {
            type = "Paper check";
            currentTransaction.setStatus(Transaction.TRANSACTION_STATUS_PROCESSED_MOBILE);
        } else {
            if (getCart().getPaymentDataType().equals(ACHData.class.getName())) {
                type = "Electronic Check";
                currentTransaction.setStatus(Transaction.TRANSACTION_STATUS_PENDING);
            } else if (getCart().getPaymentDataType().equals(CardData.class.getName())) {
                type = "Credit Card";
                currentTransaction.setStatus(Transaction.TRANSACTION_STATUS_PENDING);
            }
        }

        currentTransaction.setMobileId(cart.getMobileId());
        currentTransaction.setRecurring(recurring);
        currentTransaction.setType(type);
        currentTransaction.setCost(petitionAnswers.getCart().getItems().get(0).getCost());
    }

    private class SaveAnswersTask extends AsyncTask<String, String, String> {

        boolean saveBadCardData = false;

        public SaveAnswersTask(boolean saveBadData) {
            this.saveBadCardData = saveBadData;
        }

        @Override
        protected String doInBackground(String... params) {
            savePetitionAnswersInfo();
            saveSubjectInfo();

            try {
                paymentRedoStatus = 0;

                if (!paymentIsElectronic() || petition.getPaymentProcessorId() != 2) {
                    setCurrentTransactionStatus();
                    if (!DeviceStateUtils.isNetworkAvailable(FundSubmitActivity.this)) {
                        setTransactionStatusAndSave(Transaction.TRANSACTION_STATUS_PENDING_INTERNET);
                    }
                }

                if (getCart().getPaymentDataType() != null) {
                    setCurrentTransactionStatus();
                    if (!DeviceStateUtils.isNetworkAvailable(FundSubmitActivity.this)) {
                        setTransactionStatusAndSave(Transaction.TRANSACTION_STATUS_PENDING_INTERNET);
                    }
                }

                if (!DeviceStateUtils.isNetworkAvailable(FundSubmitActivity.this)) {
                    submitResult = SUBMIT_PENDING;
                    LOGGER.debug(String.format("PEITION ID: Submitting id as %d when no internet", petitionAnswers.getId()));
                    GrassrootsRestClient.getClient().savePetitionAnswers(petitionAnswers);
                } else if (getCart().getPaymentDataType() == null || petition.getPaymentProcessorId() != 2) {
                    submitResult = SUBMIT_OK;

                    LOGGER.debug(String.format("PEITION ID: Submitting id as %d when there is no payment.", petitionAnswers.getId()));
                    GrassrootsRestClient.getClient().savePetitionAnswers(petitionAnswers);
                }
                // For instant ACH payments or instant Credit Card Payments
                else if (getCart().getPaymentDataType() != null) {
                    processPayment(saveBadCardData);
                    return "processing";
                }

                return null;
            } catch (Exception e) {
                String errorMessage = getResources().getString(R.string.unable_to_start_rps);
                LOGGER.error("CAUGHT EXCEPTION: " + errorMessage);
                return errorMessage;

            }

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null && result.contains("processing")) {
                return;
            }
            Log.e("sk submit result", "sk submit result >>>" + submitResult);
            if (pd != null) {
                pd.dismiss();
            }

            if (submitResult == SUBMIT_PENDING) {
                if (petitionAnswers.getCart() != null) {

                }
            }

            if (submitResult == SUBMIT_OK || submitResult == SUBMIT_PENDING) {

                if (result != null) {
                    notifyUser(result);
                    logout();
                }
                onSurveySubmitted();
            }
            // electronic payment error occurred
            else {
                attemptProcessPaymentAfterFailure();
            }
        }
    }

    private void onPostPayment(String result) {
        Log.e("sk submit result", "sk submit result >>>" + submitResult);
        if (pd != null) {
            pd.dismiss();
        }

        if (submitResult == SUBMIT_OK || submitResult == SUBMIT_PENDING) {

            if (result != null) {
                notifyUser(result);
                logout();
            }
            onSurveySubmitted();
        } else if(submitResult == SERVER_ERROR) {
            setSurveyEnd();
            Log.e("cart amount", "sk  cart payment amount in if: "
                    + petitionAnswers.getCart().getItems().get(0).getCost());
            SaveAnswersTask saveAnswersTask = new SaveAnswersTask(true);
            saveAnswersTask.execute();
        }
        else {
            attemptProcessPaymentAfterFailure();
        }

    }

    private void attemptProcessPaymentAfterFailure() {

        AlertDialog.Builder dialog = new AlertDialog.Builder(FundSubmitActivity.this);
        dialog.setTitle(errorMsg).setMessage(resultMessage)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                }).setNeutralButton("Submit Anyway", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                processBadPayment();
            }
        }).setIcon(android.R.drawable.ic_dialog_alert);

        if (submitResult != SERVER_ERROR) {
            dialog.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                    paymentRedoStatus = 1;

                    if (submitResult == CREDIT_ERROR) {
                        Intent intent = new Intent(FundSubmitActivity.this, FundSwipeCardActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        startActivity(intent);
                    } else if (submitResult == ACH_ERROR) {
                        Intent intent = new Intent(FundSubmitActivity.this, FundACHActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        startActivity(intent);
                    }
                }
            });
        }

        dialog.show();

    }

    private void processBadPayment() {
        processPaymentTitle = "Submit Anyway?";
        processPaymentMsg = "The survey will be submitted, but your payment may not be processed.";
        new AlertDialog.Builder(this).setTitle(processPaymentTitle).setMessage(processPaymentMsg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        setSurveyEnd();
                        pd = ProgressDialog.show(FundSubmitActivity.this, "Please wait", "Processing Payment ...");
                        Log.e("cart amount", "sk  cart payment amount in if: "
                                + petitionAnswers.getCart().getItems().get(0).getCost());
                        SaveAnswersTask saveAnswersTask = new SaveAnswersTask(true);
                        saveAnswersTask.execute();
                    }
                }).setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        }).setIcon(android.R.drawable.ic_dialog_alert).show();
    }


    private void onSurveySubmitted() {
        GlobalData.shouldLocationReload = true;
        clearAllAnswers();
        finish();
        if (DeviceInfoUtils.isTablet(FundSubmitActivity.this)) {
            startFirstActivity(subject);
        } else {
            Intent intent = new Intent();
            intent.setClass(FundSubmitActivity.this, WalklistDetailsActivity.class);
            boolean isLocationList = false;
            if (getGlobalData().getLocation() != null) {
                isLocationList = true;
            } else {
                isLocationList = false;
            }
            if (isLocationList) {
                if (subject.getLastName() != null) {
                    intent.putExtra(WALKLIST_EXTRA_PARAMETER_LASTNAME, subject.getLastName());
                }
            } else {
                intent.putExtra(WALKLIST_EXTRA_PARAMETER_SUBJECT, subject);
            }

            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }

    private void savePetitionAnswersInfo() {
        petitionAnswers.setGpsLocation(LocationProcessor.getLastKnownGpsLocation(getApplicationContext()));
        petitionAnswers.convertPhotoFilesToAnswers(FundSubmitActivity.this);
        Subject subject = getSubject();
        subject.setKnockedStatus(KnockStatus.SUCCESS);
        petitionAnswers.mergeInAnswersAndSetIdFields(petition.getSubjectInformationQuestions(), subject);
        if (petition != null && petition.getId() != 0) {
            LOGGER.debug(String.format("PEITION ID: In savePetitonAnswersInfo() Setting to %d", petition.getId()));
            petitionAnswers.setId(petition.getId());
        } else if (petition == null) {
            LOGGER.debug(String.format("PEITION ID: In savePetitonAnswersInfo() Not setting id because petition was null"));
        } else if (petition.getId() == 0) {
            LOGGER.debug(String.format("PEITION ID: In savePetitonAnswersInfo() Id being set to 0, since petition id is set to 0"));
        }
        GlobalData.counterValue++;
    }

    private void saveSubjectInfo() {
        if (subject != null && ((petition.isWalklist()) || subject.getLocation() != null)) {
            subject.setKnockedStatus(KnockStatus.SUCCESS);
            petitionAnswers.setSubject(subject);
            List<Subject> subjects = new ArrayList<Subject>();

            subjects.add(subject);
            if (subject.getLatitude() == "0.0") {
                subject.setLatitude(null);
            }
            if (subject.getLongitude() == "0.0") {
                subject.setLongitude(null);
            }
            if (subject.getAddressLine2() != null && subject.getAddressLine2().length() == 0) {
                subject.setAddressLine2(null);
            }
            WalklistDataSource walklistdatasource = new WalklistDataSource(FundSubmitActivity.this);
            walklistdatasource.insertWalklist(subjects, subject.getLocation());
            getWalklist().setKnocked(subject);
        }
    }


    private class SaveSurveyTask extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {

            savePetitionAnswersInfo();
            saveSubjectInfo();

            try {
                Log.d("PETITION JSON:", petitionAnswers.toJson());
                LOGGER.debug(String.format("PEITION ID: Submitting id as %d", petitionAnswers.getId()));
                GrassrootsRestClient.getClient().savePetitionAnswers(
                        petitionAnswers);
                return null;
            } catch (Exception e) {
                String errorMessage = getResources().getString(R.string.unable_to_start_rps);
                LOGGER.error("CAUGHT EXCEPTION: " + errorMessage);
                return errorMessage;
            }
        }

        @Override
        protected void onPostExecute(String errorMessage) {
            super.onPostExecute(errorMessage);
            Log.e("sk submit result", "sk submit result >>>" + submitResult);
            pd.dismiss();
            if (errorMessage != null) {
                notifyUser(errorMessage);
                logout();
            }
            onSurveySubmitted();
        }
    }

    private void setTransactionStatusAndSave(String status) {
        currentTransaction.setStatus(status);
        transactionList.add(currentTransaction);
        transactionStatusPreference.set(TransactionStatusSerializer.getTransactionJson(transactionList));

        Log.d("TRANS STAT after save:", transactionStatusPreference.get());
    }

    private boolean paymentIsElectronic() {
        return getCart().getPaymentDataType() != null &&
                !getCart().isCashPayment() &&
                !getCart().isPaperCheckPayment() &&
                (getCart().getPaymentDataType().contains("ACHData")
                        || getCart().getPaymentDataType().contains("CardData"));
    }


    private int getPaymentErrorType() {
        if (getCart().getPaymentDataType().contains("ACHData")) {
            return ACH_ERROR;
        } else if (getCart().getPaymentDataType().contains("CardData")) {
            return CREDIT_ERROR;
        }
        return -1;
    }


    private ExecuteSaleData createSaleData() {
        ExecuteSaleData data = new ExecuteSaleData();
        ExecuteSaleData.AccountInfo acctInfo = new ExecuteSaleData.AccountInfo();
        ExecuteSaleData.BillingAddress billingAddress = new ExecuteSaleData.BillingAddress();

        data.setMobileSurveyId(petitionAnswers.getMobileSurveyId());
        data.setSurveyData(petitionAnswers.toJson());


        Cart cart = getCart();
        cart.setMobileId(saleTokenPreference.get());
        data.setAmount(Double.parseDouble(cart.getItems().get(0).getCost()));
        data.setCurrency("USD");
        data.setNoProcessing(cart.isNoProcessing());
        data.setScriptId(petition.getId());
        data.setProductSku(cart.getItems().get(0).getSKU());
        data.setLocalToken(saleTokenPreference.get());

        data.setOrderId(PaymentUtils.createOrderId(DeviceInfoUtils.getImei(this)));
        data.setIMEI(DeviceInfoUtils.getImei(this));
        data.setTime(Dates.getTimeStamp());


        if (getCart().getPaymentDataType().contains("ACHData")) {
            ACHData achData = (ACHData) cart.getPaymentData();
            data.setPaymentMethodType(PAYMENT_METHOD_TYPE_ACH);
            acctInfo.setAccountNumber(achData.getACHAccount());
            acctInfo.setRoutingNumber(achData.getABARouting());
        } else if (getCart().getPaymentDataType().contains("CardData")) {
            CardData cardData = (CardData) getCart().getPaymentData();
            data.setPaymentMethodType(PAYMENT_METHOD_TYPE_CC);
            acctInfo.setKns(cardData.getSerial());
            acctInfo.setEncryptedTrack(cardData.getEncTrack());
        }

        data.setAccountInfo(acctInfo);

        billingAddress.setAddress1(subject.getAddressLine1());
        billingAddress.setAddress2(subject.getAddressLine2());
        billingAddress.setCity(subject.getCity());
        billingAddress.setStateProvince(subject.getState());
        billingAddress.setEmailAddress(subject.getEmail());
        billingAddress.setPostalCode(subject.getZip());
        billingAddress.setPhoneNumber(subject.getPhone());
        billingAddress.setName(subject.getFirstName() + " " + subject.getLastName());

        data.setBillingAddress(billingAddress);
        return data;
    }

    private void onPaymentResponse(ExecuteSaleResponse response, boolean saveBadData) {

        if (response != null) {
            String responseBody = response.getResponseData();
            LOGGER.debug("LITLE RESPONSE " + responseBody);
            Tracker.appendLog("\nPayment Response: " + Tracker.getTime() + responseBody);

            try {

                if (responseBody.contains("<response>000</response>") ||
                        responseBody.contains("<response>802</response>") ||
                        responseBody.contains("<response>801</response>")
                        || saveBadData) {
                    petitionAnswers.setPaymentResponse(responseBody);
                    submitResult = SUBMIT_OK;
                    if (!saveBadData) {
                        setTransactionStatusAndSave(Transaction.TRANSACTION_STATUS_PROCESSED_MOBILE);
                    } else {
                        setTransactionStatusAndSave(Transaction.TRANSACTION_STATUS_BAD_DATA);
                    }
                    LOGGER.debug(String.format("PEITION ID: Submitting id as %d after successful payment", petitionAnswers.getId()));
                    GrassrootsRestClient.getClient().savePetitionAnswers(petitionAnswers);
                } else {   //error
                    if(response.getMessage().equals(ExecuteSaleResponseMessage.DO_NOT_HONOR.getDisplayText())
                            || response.getMessage().equals(ExecuteSaleResponseMessage.INVALID_ACCOUNT_NUMBER.getDisplayText())
                            || response.getMessage().equals(ExecuteSaleResponseMessage.INSUFFICIENT_FUNDS.getDisplayText())
                            || response.getMessage().equals(ExecuteSaleResponseMessage.GENERIC_DECLINE.getDisplayText())) {

                        errorMsg = responseBody.substring(responseBody.indexOf("<message>"), responseBody.indexOf("</message>"));
                        errorMsg = errorMsg.substring(errorMsg.indexOf(">") + 1);
                        resultMessage = "We were unable to process your payment.\n\n"
                                + "To retry, hit \"Cancel\" and resubmit.\n\n"
                                + "To give a card, paper check or cash gift instead, hit \"Cancel,\" "
                                + "back up to the contribution page, and resubmit the survey results.\n\n"
                                + "To try a different account, hit back and re-enter account information.";
                        submitResult = getPaymentErrorType();
                    }

                    else {
                        try {
                            submitResult = SERVER_ERROR;
                            setTransactionStatusAndSave(Transaction.TRANSACTION_STATUS_PENDING_FAILED);
                            LOGGER.debug(String.format("PEITION ID: Submitting id as %d after payment error", petitionAnswers.getId()));
                            GrassrootsRestClient.getClient().savePetitionAnswers(petitionAnswers);
                            LOGGER.debug(getPaymentErrorType() + " TRYING TO SUBMIT: ");
                        } catch (Exception e1) {
                            e1.printStackTrace();
                            LOGGER.debug("SUBMIT EXCEPTION: " + e1.toString());
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                String errorType = (getPaymentErrorType() == ACH_ERROR) ?
                        "ACH EXCEPTION" : "CARD EXCEPTION";               //ach or card error
                LOGGER.debug(errorType + e.toString());
                try {
                    submitResult = SUBMIT_OK;
                    setTransactionStatusAndSave(Transaction.TRANSACTION_STATUS_PENDING_FAILED);
                    LOGGER.debug(String.format("PEITION ID: Submitting id as %d after payment error", petitionAnswers.getId()));
                    GrassrootsRestClient.getClient().savePetitionAnswers(petitionAnswers);
                    LOGGER.debug(errorType + " TRYING TO SUBMIT: " + e.toString());
                } catch (Exception e1) {
                    e1.printStackTrace();
                    LOGGER.debug("SUBMIT EXCEPTION: " + e1.toString());
                }
            }

            Tracker.appendLog("\nOutgoing JSON: " + Tracker.getTime() + petitionAnswers.toJson().toString());

        } else {
            try {
                submitResult = SUBMIT_OK;
                setTransactionStatusAndSave(Transaction.TRANSACTION_STATUS_PENDING_INTERNET);
                LOGGER.debug(String.format("PEITION ID: Submitting id as %d after payment error", petitionAnswers.getId()));
                GrassrootsRestClient.getClient().savePetitionAnswers(petitionAnswers);
            } catch (Exception e1) {
                e1.printStackTrace();
                LOGGER.debug("SUBMIT EXCEPTION: " + e1.toString());
            }

            //onPostPayment(null);
        }

        onPostPayment(null);


    }

    private void onPaymentFail(Throwable error, boolean saveBadData) {
        Log.d("RAND", "Something not good");
        LOGGER.error("Payment fail", error);
        submitResult = SUBMIT_OK;
        errorMsg = "Server Error";
        resultMessage = error.getMessage();

        if (saveBadData) {
            submitResult = SUBMIT_OK;
        }

        onPaymentResponse(null, saveBadData);
    }

    private void processPayment(boolean saveBadData) {
        if (!paymentIsElectronic()) return;


        String authHeader = authenticationHeaderPreference.get();
        String imei = DeviceInfoUtils.getImei(this);
        Observable<ExecuteSaleResponse> saleResponse = restService
                .executeSale(imei, authHeader, createSaleData());

        saleResponse.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        payment -> onPaymentResponse(payment, saveBadData),
                        error -> onPaymentFail(error, saveBadData));

    }

    private void initTransactionList() {
        String transactionJson = transactionStatusPreference.get();

        Log.d("TRANSACTION STRING: ", transactionJson);
        if (!transactionStatusPreference.isSet() || transactionJson.equals("")) {
            transactionList = new ArrayList<>();
        } else {
            transactionList = TransactionStatusSerializer.getTransactionList(transactionStatusPreference.get());
        }
    }

    private SubmitView.ViewListener viewListener = new SubmitView.ViewListener() {
        @Override
        public void onNext() {
            FundSubmitActivity.this.onNext();
        }

        @Override
        public void onMissingSubjectInfo(Subject subject) {
            FundSubmitActivity.this.onMissingSubjectInfo(subject);
        }

        @Override
        public void onMissingQuestionAnswer(Question question) {
            FundSubmitActivity.this.onMissingQuestionAnswer(question);
        }
    };

    protected void onMissingSubjectInfo(Subject subject) {
        ConfirmCancelDialogBuilder.createMissingSubjectInfoDialog(this, subject,
                petition.getSubjectInformationQuestions(), new ConfirmCancelDialogBuilder.ViewListener<Subject>() {
                    @Override
                    public void onConfirm(Subject newSubject) {
                        petitionAnswers.mergeInAnswersAndSetIdFields(petition.getSubjectInformationQuestions(),
                                newSubject);
                        petitionAnswers.setId(petition.getId());
                        view.setSubject(newSubject, petition);
                        petitionAnswers.setSubject(newSubject);
                    }

                    @Override
                    public void onCancel() {

                    }
                }).show();
    }

    /**
     * Constructs a dialog so a user can enter an answer for an unanswered
     * question. Shows the question title, the question text (long questions are
     * truncated), and the available answers for the question.
     *
     * @param question The question to be displayed
     */
    protected void onMissingQuestionAnswer(Question question) {
        // Build dialog & inflate view
        AlertDialog.Builder missingQuestionAnswerDialogBuilder = ConfirmCancelDialogBuilder
                .getAlertDialogBuilder(FundSubmitActivity.this);
        missingQuestionView = (QuestionView) View.inflate(FundSubmitActivity.this, R.layout.question, null);
        missingQuestionView
                .setMaxQuestionHeight(getResources().getInteger(R.integer.max_question_dialog_height_in_pixels));

        // Set photo view listener
        if (question.isPicture()) {
            missingQuestionView.setPhotoViewListener(getPhotoViewListener());
        }

        // Set title & hide standard header
        missingQuestionAnswerDialogBuilder.setTitle(question.getName());
        View header = missingQuestionView.findViewById(R.id.header_title);
        header.setVisibility(View.GONE);

        // Configure buttons
        missingQuestionAnswerDialogBuilder.setPositiveButton(R.string.ok, getRegularQuestionListener());
        missingQuestionAnswerDialogBuilder.setNegativeButton(R.string.cancel, getNegativeClickListener());

        // Set view & question data
        missingQuestionView.setPetitionAnswers(petitionAnswers);
        missingQuestionView.setQuestion(question);
        missingQuestionAnswerDialogBuilder.setView(missingQuestionView);

        // Show dialog
        AlertDialog dialog = missingQuestionAnswerDialogBuilder.create();
        dialog.show();
    }
}
