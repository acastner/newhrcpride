package com.grassroots.funds.activities;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.grassroots.funds.R;
import com.grassroots.funds.models.FundQuestions;
import com.grassroots.petition.activities.BasePetitionActivity;
import com.grassroots.petition.models.Question;

public class FundDisclosueLanguageActivity extends BasePetitionActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		bOverrideDefaultAutoOrientation = true;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.disclosurelanguage);
		TextView disclosureText = (TextView) findViewById(R.id.disclaimer_text);
		Question question = petition.getQuestionWithVarName(FundQuestions.SIGNATURE_CONSENT);
		String disclaimerText =question.getText();
		//disclosureText.setText(disclaimerText);

		setRequestedOrientation(android.content.pm.ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		Button nextButton = (Button) findViewById(R.id.next_button);
		nextButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();		
			}
		});

	}
}
