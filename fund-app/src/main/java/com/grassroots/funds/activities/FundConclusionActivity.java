package com.grassroots.funds.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.grassroots.funds.R;
import com.grassroots.funds.models.FundQuestions;
import com.grassroots.petition.activities.BasePetitionActivity;
import com.grassroots.petition.activities.ConclusionEndExplanationActivity;
import com.grassroots.petition.activities.ConclusionReasonForRefusalActivity;
import com.grassroots.petition.activities.SubmitActivity;
import com.grassroots.petition.activities.TerminateQuestionsActivity;
import com.grassroots.petition.models.Cart;
import com.grassroots.petition.models.Petition;
import com.grassroots.petition.models.Question;
import com.grassroots.petition.models.SubjectAnswer;
import com.grassroots.petition.views.QuestionView;
import com.grassroots.petition.views.QuestionsView;

public class FundConclusionActivity extends BasePetitionActivity {
	 private QuestionsView view;
	 private QuestionView questionview;
	 private Question question;
	 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		question = petition.getQuestionWithVarName(FundQuestions.FUNDCONCLUSION);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		view = (QuestionsView) View.inflate(this, R.layout.questions, null);
	       view.setPetitionAnswers(petitionAnswers);
	       view.setQuestion(question);
	       setContentView(view);
	       questionview = (QuestionView)(findViewById(R.id.question));
	       questionview.setQuestion(question);
	       hideMarketingIfHasNoMaterials();
        configureBackButton();
        configureNextButton();
        //configureMarketingMaterialsButton();
	}

   private void configureMarketingMaterialsButton ()
	    {
	        findViewById( R.id.marketing_material_button ).setOnClickListener( new View.OnClickListener()
	        {
	            @Override
	            public void onClick (View view)
	            {
	                onMarketing();
	            }
	        } );
	    }

	private boolean hasPayment() {
		Cart cart = petitionAnswers.getCart();
		if(cart != null && (cart.isCashPayment() || cart.isPaperCheckPayment() || cart.getPaymentDataType() != null)) {
			return true;
		}
		return false;
	}
	
	/**
     * Configures next button.
     */
    private void configureNextButton ()
    {
    	
        findViewById( R.id.next_button ).setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick (View v)
            {
            	SubjectAnswer answer = questionview.getAnswer();
            	
				if(question.isRequired() && !answer.hasAnswer()){
					Toast.makeText(FundConclusionActivity.this, "This question is required", Toast.LENGTH_LONG).show();
					return;
				}else{
					petitionAnswers.overwriteAnswer(answer);
				}

				//TODO: Acutally implement
				boolean shouldShowReasonForRefusal = !hasPayment();

				if(shouldShowReasonForRefusal) {
					Intent endExpl = new Intent(FundConclusionActivity.this, ConclusionReasonForRefusalActivity.class);
					startActivityForResult(endExpl, REFUSAL_REQUEST);
				} else {
					Intent subjectreview = new Intent(FundConclusionActivity.this, FundSubmitActivity.class);
					startActivity(subjectreview);
				}

            }
        } );
    }

    public static final int REFUSAL_REQUEST = 21;

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// Check which request we're responding to
		if (requestCode == REFUSAL_REQUEST) {
			// Make sure the request was successful
			if (resultCode == RESULT_OK) {

				Intent subjectreview = new Intent(FundConclusionActivity.this, FundSubmitActivity.class);
				startActivity(subjectreview);
				// The user picked a contact.
				// The Intent's data Uri identifies which contact was selected.

				// Do something with the contact here (bigger example below)
			}
		}
	}
    
    /**
     * Configures back button.
     */
    private void configureBackButton ()
    {
        findViewById( R.id.back_button ).setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick (View v)
            {
                onBackPressed();
            }
        } );
    }
    
    public static boolean shouldProceedWithPetition (Petition petition)
    {
        return true;
    }
}
