package com.grassroots.funds.activities;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.grassroots.funds.R;
import com.grassroots.funds.models.FundQuestions;
import com.grassroots.petition.activities.BaseCreditCardSwipeActivity;
import com.grassroots.petition.models.Cart;
import com.grassroots.petition.models.ProductOrder;
import com.grassroots.petition.models.Question;
import com.grassroots.petition.models.Subject;
import com.grassroots.petition.models.SubjectAnswer;
import com.grassroots.petition.utils.Strings;

public class FundSwipeCardActivity extends BaseCreditCardSwipeActivity {

	private String[] years;
	private String[] months;
	private String[] types;
	private Spinner creditCardExpMonthSpinner,creditCardExpYearSpinner;


	protected EditText cardExpiration;
	protected Spinner creditCardTypeSpinner;
	protected String selectedCreditCardType;

	@Override
	public void onCreate(Bundle savedInstanceState){

		super.onCreate(savedInstanceState);

		((TextView) findViewById(R.id.question_page_header_title)).setText(R.string.credit_card_data_header_title);        
		creditCardTypeSpinner = (Spinner) findViewById(R.id.credit_card_type);
		types = getResources().getStringArray(R.array.credit_card_types);

		ArrayAdapter<String> typeAdapter = new ArrayAdapter<String>(
				this, R.layout.spinner_layout, types);
		typeAdapter.setDropDownViewResource(R.layout.spinner_layout);
		creditCardTypeSpinner.setAdapter(typeAdapter);
		creditCardTypeSpinner.setOnItemSelectedListener(new CustomOnItemSelectedListener());

		creditCardExpMonthSpinner = (Spinner) findViewById(R.id.credit_card_exp_month_spinner);
		creditCardExpYearSpinner = (Spinner) findViewById(R.id.credit_card_exp_year_spinner);
		setCreditCardExpirationView();

	}


	private void setCreditCardExpirationView() {
		months = new String[]{"01","02","03","04","05","06","07","08","09","10","11","12"};
		formatYears();
		ArrayAdapter<String> monthadapter = new ArrayAdapter<String>(
				this, R.layout.spinner_layout, months);
		ArrayAdapter<String> yearadapter = new ArrayAdapter<String>(
				this, R.layout.spinner_layout, years);

		monthadapter.setDropDownViewResource(R.layout.spinner_layout);
		yearadapter.setDropDownViewResource(R.layout.spinner_layout);

		creditCardExpMonthSpinner.setAdapter(monthadapter);
		creditCardExpYearSpinner.setAdapter(yearadapter);
	}



	@Override
	public void onNext() {
		if (cardData == null) {
			notifyUser(getString(R.string.SWIPE_YOUR_CREDIT_CARD_MESSAGE));
			return;
		}

		petitionAnswers = getGlobalData().getPetitionAnswers();
		int monthSelected = Integer.parseInt(creditCardExpMonthSpinner.getSelectedItem().toString());
		int currentMonth = Calendar.getInstance().get(Calendar.MONTH)+1;
		int currentyear = Calendar.getInstance().get(Calendar.YEAR);
		int yearSelected = Integer.parseInt(creditCardExpYearSpinner.getSelectedItem().toString());

		if(currentyear == yearSelected && monthSelected<currentMonth){
			notifyUser("Please input a valid credit card expiration date");
			return;
		}

		String stringAnswer = creditCardExpMonthSpinner.getSelectedItem().toString()+creditCardExpYearSpinner.getSelectedItem().toString();        
		if(stringAnswer == null || Strings.isEmpty(stringAnswer))        
		{
			notifyUser("Please input the Credit Card Expiration Date");
			return;
		}
		else if(!isLegalExpirationDate(stringAnswer))
		{
			notifyUser("Please input the Credit Card Expiration Date");
			return;
		}
		else
		{        	
			Question question = petition.getQuestionWithVarName(FundQuestions.CREDIT_CARD_EXPIRATION_DATE);     	
			SubjectAnswer answer = new SubjectAnswer(question.getId(), stringAnswer);
			petitionAnswers.overwriteAnswer(answer);	
		}

		if(selectedCreditCardType == null || selectedCreditCardType.contains("No Selection")
				|| creditCardTypeSpinner.getSelectedItemPosition() == 0)
		{			
			notifyUser("Please select a type of credit card from the dropdown.");
			return;
		}
		else
		{			
			Question question = petition.getQuestionWithVarName(FundQuestions.CREDIT_CARD_TYPE);     	
			SubjectAnswer answer = new SubjectAnswer(question.getId(), selectedCreditCardType);
			petitionAnswers.overwriteAnswer(answer);
		}

		Cart cart = getCart();
		if (null == cart || 0 == cart.getItems().size()) {
			notifyUser(getString(R.string.NO_PRODUCT_WAS_SELECTED_MESSAGE));
			return;
		}

		swipeView.closeReader();

		cart.setPaymentData(cardData);

		String renewal = getSubject().getOtherFieldsMapping().get("CARDTYPE");
		if (renewal != null && (renewal.equalsIgnoreCase("Sustainer Upgrade") || renewal.equalsIgnoreCase("AR Upgrade"))) {
			cart.setNoProcessing(true);
		}

		List<ProductOrder> productorder = getCart().getItems();
		String sku = productorder.get(0).getSKU();
		Log.e("sku","sku : "+sku);



		if(FundSubmitActivity.paymentRedoStatus == 1)
		{
			FundSubmitActivity.paymentRedoStatus = 0;
			startActivity(new Intent(FundSwipeCardActivity.this,FundSubmitActivity.class));
		}
		else
		{
			startActivity(new Intent(FundSwipeCardActivity.this,FundACHConsentActivity.class));
		}

	}

	private boolean isLegalExpirationDate(String strdate)
	{
		String strFormat = "MMyyyy";
		try
		{
			SimpleDateFormat sdf = new SimpleDateFormat(strFormat);
			sdf.setLenient(false);
			Date date = sdf.parse(strdate,new ParsePosition(0));
			if (date == null)
			{
				notifyUser("Could not parse date entered.");
				return false;
			}
			SimpleDateFormat dtYear = new SimpleDateFormat("yyyy");
			int year = Integer.parseInt(dtYear.format(date));
			if (year<1900 || year > 2100)
			{
				notifyUser("Invalid year "+year + " year cannot be greater than 2100 or less than 1900");
				return false;
			}
			return true;
		}		
		catch(IllegalArgumentException e)
		{
			return false;
		}
	}

	public class CustomOnItemSelectedListener implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {
			selectedCreditCardType = parent.getItemAtPosition(pos).toString();
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			selectedCreditCardType = null;
		}

	}

	/**
	 * format year range for card expiration
	 */
	private void formatYears(){
		Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);

		ArrayList<String> yearList = new ArrayList<String>();
		for(int i=0; i<10; i++){
			yearList.add(""+year);
			year++;
		}
		years = new String[yearList.size()];
		years = yearList.toArray(years);

	}

	@Override
	public void onResume() {
		super.onResume();
	}
	
	@Override
    public void onBackPressed()
    {       
		if(FundSubmitActivity.paymentRedoStatus == 1)
		{
			FundSubmitActivity.paymentRedoStatus = 0;
			startActivity(
					new Intent(FundSwipeCardActivity.this,FundSubmitActivity.class)
					.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
		}
		else
		{
			super.onBackPressed();
		}
       
    }
	

}


