package com.grassroots.funds.activities;

import com.grassroots.funds.models.FundQuestions;
import com.grassroots.petition.R;
import com.grassroots.petition.activities.BasePetitionActivity;
import com.grassroots.petition.models.Question;
import com.grassroots.petition.models.SubjectAnswer;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class PACAbove25Activty extends BasePetitionActivity {

	private Question question;
	private Button nextButton, backButton, noThanksButton;
	private RadioGroup percentageSelectGroup;

	private Button button75, button50, button25, button10;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pacabove25);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		question = petition.getQuestionWithVarName(Question.PACABOVE25);

		button75 = (Button) findViewById(R.id.percent_75_button);
		button50 = (Button) findViewById(R.id.percent_50_button);
		button25 = (Button) findViewById(R.id.percent_25_button);
		button10 = (Button) findViewById(R.id.percent_10_button);
		
		nextButton = (Button) findViewById(R.id.next_button);
		backButton = (Button) findViewById(R.id.back_button);
		noThanksButton = (Button) findViewById(R.id.nothanks_button);

		button75.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				savePercentQuestionAnswer("75%");
			}
		});

		button50.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				savePercentQuestionAnswer("50%");
			}
		});

		button25.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				savePercentQuestionAnswer("25%");
			}
		});

		button10.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				savePercentQuestionAnswer("10%");
			}
		});


		backButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
		
		noThanksButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String percentage = "0%";
				SubjectAnswer subjectAnswer = new SubjectAnswer(question.getId(), percentage);
				petitionAnswers.addAnswer(subjectAnswer);
				startActivity( getNextActivityAfterActivityClass(FundDonationActivity.class) );
			}
		});
	}

	@Override
	public void onBackPressed() {
		try {
			petitionAnswers.removeAnswer(petition.getQuestionWithVarName(Question.PACABOVE25));
		} catch(Exception ex) {

		}
		super.onBackPressed();
	}

	private void savePercentQuestionAnswer(String percentage) {
		SubjectAnswer subjectAnswer = new SubjectAnswer(question.getId(), percentage);
		petitionAnswers.addAnswer(subjectAnswer);
		Question occupationQuestion = petition.getQuestionWithVarName(FundQuestions.OCCUPATION);
		Question employerQuestion = petition.getQuestionWithVarName(FundQuestions.EMPLOYER);
		Question disclaimerQuestion = petition.getQuestionWithVarName(FundQuestions.PAC_DISCLAIMER);

		if(disclaimerQuestion != null) {
			startActivity(PACDisclaimerActivity.class);
		} else if(occupationQuestion != null && employerQuestion != null) {
			startActivity( FundOccupationEmployerActivity.class );
		} else {
			startActivity( getNextActivityAfterActivityClass(FundDonationActivity.class) );
		}

	}

}
