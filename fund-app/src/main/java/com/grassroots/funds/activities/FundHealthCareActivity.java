package com.grassroots.funds.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.grassroots.funds.R;
import com.grassroots.funds.models.FundQuestions;
import com.grassroots.petition.activities.BasePetitionActivity;
import com.grassroots.petition.activities.BaseSubjectInfoActivity;
import com.grassroots.petition.activities.SubmitActivity;
import com.grassroots.petition.models.Petition;
import com.grassroots.petition.models.Question;
import com.grassroots.petition.models.Subject;
import com.grassroots.petition.models.SubjectAnswer;
import com.grassroots.petition.views.QuestionView;
import com.grassroots.petition.views.QuestionsView;

public class FundHealthCareActivity extends BasePetitionActivity {
	 private QuestionsView view;
	 private QuestionView questionview;
	 private Question question;
	 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		question = petition.getQuestionWithVarName(FundQuestions.FLAG_HEALTHCAREPROVIDER);
		view = (QuestionsView) View.inflate(this, R.layout.questions, null);
	       view.setPetitionAnswers(petitionAnswers);
	       view.setQuestion(question);
	       setContentView(view);
	       questionview = (QuestionView)(findViewById(R.id.question));
	       questionview.setQuestion(question);
	       hideMarketingIfHasNoMaterials();
        configureBackButton();
        configureNextButton();
        configureMarketingMaterialsButton();
	}
	
	   private void configureMarketingMaterialsButton ()
	    {
	        findViewById( R.id.marketing_material_button ).setOnClickListener( new View.OnClickListener()
	        {
	            @Override
	            public void onClick (View view)
	            {
	                onMarketing();
	            }
	        } );
	    }
	
	/**
     * Configures next button.
     */
    private void configureNextButton ()
    {
		findViewById( R.id.next_button ).setVisibility(View.GONE);
        findViewById( R.id.next_button ).setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick (View v)
            {
            	SubjectAnswer answer = questionview.getAnswer();
            	
				if(question.isRequired() && !answer.hasAnswer()){
					Toast.makeText(FundHealthCareActivity.this, "This question is required", Toast.LENGTH_LONG).show();
					return;
				}else{
					petitionAnswers.overwriteAnswer(answer);
				}
				startActivity(FundConclusionActivity.class);
            }
        } );
    }
    
    /**
     * Configures back button.
     */
    private void configureBackButton ()
    {
        findViewById( R.id.back_button ).setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick (View v)
            {
                onBackPressed();
            }
        } );
    }
    
    public static boolean shouldProceedWithPetition (Petition petition)
    {
        return true;
    }

}
