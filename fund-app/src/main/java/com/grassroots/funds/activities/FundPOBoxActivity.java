package com.grassroots.funds.activities;

import com.grassroots.funds.R;
import com.grassroots.funds.models.FundQuestions;
import com.grassroots.petition.activities.BasePetitionActivity;
import com.grassroots.petition.models.Question;
import com.grassroots.petition.models.SubjectAnswer;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class FundPOBoxActivity extends BasePetitionActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.po_box);
		
		 TextView headerTitle = (TextView) findViewById(R.id.header_title);
	        if (null != headerTitle) {
	            headerTitle.setText(getString(R.string.pobox));
	        }
	        
		Button nextButton = (Button) findViewById(R.id.next_button);
		Button partnerInfoButton = (Button) findViewById(R.id.partnerInfo_button);
		
		if(petition != null && petition.getAppThemeColor() != null)
		{
			partnerInfoButton.setBackgroundColor(Color.parseColor("#" + petition.getAppThemeColor()));
		}

		nextButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Question poboxquestion = petition.getQuestionWithVarName(FundQuestions.POBOX);
				TextView poboxView = (TextView) findViewById(R.id.pobox_edit);
				if(poboxquestion.isRequired() && poboxView.getText().toString().length()==0){
					Toast.makeText(FundPOBoxActivity.this, "Please enter PO Box", Toast.LENGTH_LONG).show();
					return;
				}else{
				    petitionAnswers.overwriteAnswer(new SubjectAnswer(poboxquestion.getId(), poboxView.getText().toString()));
				}
				
				Question poboxCityquestion = petition.getQuestionWithVarName(FundQuestions.POBOX_CITY);
				TextView poboxCityView = (TextView) findViewById(R.id.pocity_edit);
				
				if(poboxCityquestion.isRequired() && poboxCityView.getText().toString().length()==0){
					Toast.makeText(FundPOBoxActivity.this, "Please enter PO Box City", Toast.LENGTH_LONG).show();
					return;
				}else{
					petitionAnswers.overwriteAnswer(new SubjectAnswer(poboxCityquestion.getId(), poboxCityView.getText().toString()));
				}
				
				Question poboxStatequestion = petition.getQuestionWithVarName(FundQuestions.POBOX_STATE);
				TextView poboxStateView = (TextView) findViewById(R.id.postate_edit);
				if(poboxStatequestion.isRequired() && poboxStateView.getText().toString().length()==0){
					Toast.makeText(FundPOBoxActivity.this, "Please enter PO Box State", Toast.LENGTH_LONG).show();
					return;
				}else{
					petitionAnswers.overwriteAnswer(new SubjectAnswer(poboxStatequestion.getId(), poboxStateView.getText().toString()));
				}
				
				Question poboxZipquestion = petition.getQuestionWithVarName(FundQuestions.POBOX_ZIP);
				TextView poStateView = (TextView) findViewById(R.id.pozip_edit);
				if(poboxZipquestion.isRequired() && poStateView.getText().toString().length()==0){
					Toast.makeText(FundPOBoxActivity.this, "Please enter PO Box Zip", Toast.LENGTH_LONG).show();
					return;
				}else{
					petitionAnswers.overwriteAnswer(new SubjectAnswer(poboxZipquestion.getId(), poStateView.getText().toString()));
				}				
				startActivity(getNextActivityAfterActivityClass(FundSubjectInfoActivity.class));
				
			}
		});
		
		partnerInfoButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {

				Intent intent = new Intent(FundPOBoxActivity.this,FundPartnerInfoActivity.class);
				startActivity(intent);
			}
		});
	}
}
