package com.grassroots.funds.activities;

import android.app.Activity;
import android.content.pm.ActivityInfo;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.grassroots.funds.R;
import com.grassroots.funds.models.FundQuestions;
import com.grassroots.petition.activities.BasePetitionActivity;
import com.grassroots.petition.activities.BasePetitionActivityWithMenu;
import com.grassroots.petition.models.Question;

public class FundMobileActionTermsActivity extends BasePetitionActivity {

	private static final String TAG = FundMobileActionTermsActivity.class.getName();
	private static final Logger LOGGER = Logger.getLogger(TAG);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mobile_terms_and_conditions);

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		TextView headerTitle = (TextView) findViewById(R.id.header_title);
		headerTitle.setText("Terms and Conditions");

		configureBackButton();

		String termsText = "";
		JSONObject jsonObj = petition.getPetitionJSON();
		JSONArray jsonArr = new JSONArray();
		try {
			jsonArr = jsonObj.getJSONArray("ScriptProperties");

			for (int i = 0; i < jsonArr.length(); i++) {

				if (jsonArr.getJSONObject(i).getString("Name").equals("termsAndConditions")) {
					termsText = jsonArr.getJSONObject(i).getString("Value");
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		TextView terms = (TextView) findViewById(R.id.terms_text);
//		if (!termsText.equals("")) {
//			terms.setText(termsText);
//		} else {
//			terms.setText("Problem retreiving terms and conditions");
//		}
	}

	/**
	 * Configures back button.
	 */
	private void configureBackButton() {
		findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
	}
}
