package com.grassroots.funds.activities;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.grassroots.funds.R;
import com.grassroots.funds.models.FundQuestions;
import com.grassroots.petition.activities.BasePetitionActivity;
import com.grassroots.petition.models.Question;
import com.grassroots.petition.models.SubjectAnswer;

/**
 * Created by jason on 1/11/2017.
 */

public class PACDisclaimerActivity extends BasePetitionActivity {

    private Question question;
    private Button nextButton, backButton;
    private CheckBox disclaimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.grassroots.petition.R.layout.pac_disclaimer_activity);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        nextButton = (Button) findViewById(com.grassroots.petition.R.id.next_button);
        backButton = (Button) findViewById(com.grassroots.petition.R.id.back_button);

        disclaimer = (CheckBox) findViewById(R.id.pacDisclaimerCheckbox);

        disclaimer.setChecked(false);

        nextButton.setOnClickListener(v -> {
            if(disclaimer.isChecked()){
                Question occupationQuestion = petition.getQuestionWithVarName(FundQuestions.OCCUPATION);
                Question employerQuestion = petition.getQuestionWithVarName(FundQuestions.EMPLOYER);

                if(occupationQuestion != null && employerQuestion != null)
                {
                    startActivity( FundOccupationEmployerActivity.class );
                } else {
                    startActivity( getNextActivityAfterActivityClass(FundDonationActivity.class) );
                }
            }else{
                Toast.makeText(PACDisclaimerActivity.this, "Please, check that you have read and understood the requirements.", Toast.LENGTH_LONG ).show();;
            }
        });

        backButton.setOnClickListener(v -> onBackPressed());
    }


}
