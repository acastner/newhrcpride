package com.grassroots.funds.activities;

import org.apache.log4j.Logger;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.grassroots.funds.R;
import com.grassroots.petition.activities.QuestionsActivity;
import com.grassroots.petition.models.Question;
import com.grassroots.petition.models.SubjectAnswer;
import com.grassroots.petition.views.QuestionView;

public class FundQuestionsActivity extends QuestionsActivity {
	// private static final String TAG = FundQuestionsActivity.class.getName();
	private static final String TAG = "XENLOG";
	private static final Logger LOGGER = Logger.getLogger(TAG);
	WebView webView = null;
	protected boolean backButtonPressed = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		LOGGER.warn("FundQuestionsActivity oncreate called");
		addRadioButtonListeners();
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		webView = (WebView) findViewById(R.id.question_formatted_webview);
		WebSettings webSettings = webView.getSettings();
		webSettings.setDefaultFontSize(20);
		
		Button backbutton = (Button) findViewById(R.id.back_button);
		backbutton.setOnClickListener(v -> {
            backButtonPressed  = true;
            webView.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageFinished(WebView view, String url) {
                    if(backButtonPressed){
                    LOGGER.warn("Loading of empty page completed. Calling onNext()");
                    backButtonPressed = false;
                    onNext();
                    }
                }
            });
            webView.loadData("<HTML><BODY>&nbsp;</BODY></HTML>","text/html",null);
        });
	}
	
	

	@Override
	public void onResume() {
		super.onResume();
		addRadioButtonListeners();
	}

	@Override
	protected void onNext(SubjectAnswer answer) {
		super.onNext(answer);
		addRadioButtonListeners();

	}

	@Override
	protected void onNext() {
		super.onNext();
		addRadioButtonListeners();
	}

	private static final String VOLUNTEER = "VOLUNTEER";
	private static final String ISSUES = "ISSUES";
	private static final String FLAG_HEALTHCAREPROVIDER = "FLAG_HEALTHCAREPROVIDER";

	private void addRadioButtonListeners() {
		/*
		  Special for FUND JOB 7, remove next button for ISSUES and VOLUNTEER QUESTIONS
		 */
		Question question = super.getCurrentQuestion();
		String varName = question.getVarNameNo$();
		if(varName.equals(VOLUNTEER) || varName.equals(ISSUES) || varName.equals(FLAG_HEALTHCAREPROVIDER)) {
			findViewById(R.id.next_button).setVisibility(View.GONE);
		} else {
			findViewById(R.id.next_button).setVisibility(View.VISIBLE);
		}



		final RadioGroup multipleChoiceColumn1 = (RadioGroup) findViewById(R.id.multiple_choice_1);
		final RadioGroup multipleChoiceColumn2 = (RadioGroup) findViewById(R.id.multiple_choice_2);


		OnClickListener clickListener = v -> {
            LOGGER.warn("FundQuestionsActivity radion group button pressed");
            QuestionView questionView = (QuestionView) findViewById(R.id.question);
            onNext(questionView.getAnswer());
        };
		OnClickListener column1ClickListener = v -> {
            LOGGER.warn("FundQuestionsActivity multi radio group button pressed");
            multipleChoiceColumn2.clearCheck();
            QuestionView questionView = (QuestionView) findViewById(R.id.question);
            onNext(questionView.getAnswer());
        };
		OnClickListener column2ClickListener = v -> {
            LOGGER.warn("FundQuestionsActivity multi radio group button pressed");
            multipleChoiceColumn1.clearCheck();
            QuestionView questionView = (QuestionView) findViewById(R.id.question);
            onNext(questionView.getAnswer());
        };
		addListener((RadioGroup) findViewById(R.id.multiple_choice),
				clickListener);
		addListener(multipleChoiceColumn1, column1ClickListener);
		addListener(multipleChoiceColumn2, column2ClickListener);
	}

	private void addListener(RadioGroup radioGroup,
			OnClickListener clickListener) {
		int count = radioGroup.getChildCount();
		for (int i = 0; i < count; i++) {
			View o = radioGroup.getChildAt(i);
			if (o instanceof RadioButton) {
				RadioButton button = (RadioButton) o;
				button.setOnClickListener(clickListener);
			}
		}
	}

}
