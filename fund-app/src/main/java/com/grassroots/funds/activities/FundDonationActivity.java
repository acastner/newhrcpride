package com.grassroots.funds.activities;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.grassroots.funds.R;
import com.grassroots.funds.models.FundQuestions;
import com.grassroots.petition.activities.BaseDonationActivity;
import com.grassroots.petition.db.helper.WalklistDataSource;
import com.grassroots.petition.models.GpsLocation;
import com.grassroots.petition.models.KnockEvent;
import com.grassroots.petition.models.KnockStatus;
import com.grassroots.petition.models.Knocks;
import com.grassroots.petition.models.Product;
import com.grassroots.petition.models.Question;
import com.grassroots.petition.models.Subject;
import com.grassroots.petition.models.SubjectAnswer;
import com.grassroots.petition.services.GrassrootsRestClient;
import com.grassroots.petition.utils.LocationProcessor;
import com.grassroots.petition.views.HomeStatusView;

public class FundDonationActivity extends BaseDonationActivity {

	private Subject subject;
	private String recurringType,paymentMethod;
	private boolean isAnalog;
	public String RECURRING_ONE_TIME = "ONE-TIME";
	public static boolean donation;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);



		donation = false;
		
		Button knockButton = (Button) findViewById(R.id.knock_button);
		knockButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.e("knock button","knock---");
				showStatusSelectionDialog();
			}
		});		
	}
	
	
	@Override
	public void onResume()
	{
		super.onResume();
		Question question = petition.getQuestionWithVarName(FundQuestions.CREDIT_CARD_EXPIRATION_DATE);
		petitionAnswers.removeAnswer(question);
		question = petition.getQuestionWithVarName(FundQuestions.CREDIT_CARD_TYPE);
		petitionAnswers.removeAnswer(question);
	}
	

	/**
	 * Display a dialog with a list of available knock statuses where the user can select appropriate knock status for the selected subjects address.
	 */
	protected void showStatusSelectionDialog () {

		final Dialog statusDialog = new Dialog(this);
		statusDialog.setTitle(R.string.home_status);
		HomeStatusView view = (HomeStatusView) View.inflate(this, R.layout.home_status, null);

		view.setViewListener(new HomeStatusView.ViewListener() {
			@Override
			public void onStatusSelected(KnockStatus knockStatus) {
				subject = getSubject();
				//Once status is selected intiate a knock event
				GpsLocation location = LocationProcessor.getLastKnownGpsLocation(getApplicationContext());

				KnockEvent knockEvent = null;
				if(subject.getHid() == null){
					subject = getSubject();
					knockEvent = new KnockEvent(knockStatus, location, subject.getFirstName(), subject.getLastName(),
							subject.getAddressLine1(), subject.getAddressLine2(), subject.getCity(), subject.getState(), 
							subject.getZip(), subject.getPhone(), subject.getEmail(),getGlobalData().getLocation(),"");
				}else{
					try{
						location.setLatitude(Double.parseDouble(subject.getLatitude()));
						location.setLongitude(Double.parseDouble(subject.getLongitude()));
					}catch(NumberFormatException e){
						e.printStackTrace();
					}
					knockEvent = new KnockEvent(Integer.parseInt(subject.getHid()), knockStatus, location);
				}

				Knocks knocks = new Knocks();
				knockEvent = addBatteryInfoToKnock(knockEvent);
				knocks.addKnockEvent(knockEvent);
				try {
					WalklistDataSource walklistDataSource = new WalklistDataSource(FundDonationActivity.this);
					List<Subject> subjects = new ArrayList<Subject>();
					subject.setKnockedStatus(knockStatus);
					subject.setLatitude(Double.toString(location.getLatitude()));
					subject.setLongitude(Double.toString(location.getLongitude()));
					subjects.add(subject);
					walklistDataSource.insertWalklist(subjects,subject.getLocation());
					setSubject(subject);
					GrassrootsRestClient.getClient().saveKnocks(knocks);
					startFirstActivity(subject);
				} catch (Exception exception) {
					logoutWithError( getString( R.string.error_unable_to_send_knock_status ), exception );   
				}


				statusDialog.cancel();
			}



			@Override
			public void onCancel() {
				statusDialog.cancel();
			}
		});

		statusDialog.setContentView(view);
		statusDialog.show();
	}

	@Override
	protected void onSkip() {
		Log.e("going to skip donation","skip donation go to pledge");
		//if card type is sustainer upgrade skip annual
		String renewal = getSubject().getOtherFieldsMapping().get("CARDTYPE");
		if(renewal != null && renewal.equalsIgnoreCase("Sustainer Upgrade")){
			startActivity( getNextActivityAfterActivityClass(FundDonationActivity.class) );
		}else{
			startActivity(new Intent(FundDonationActivity.this,FundAnnualDonationActivity.class));
		}

	}

	@Override
	protected void handlePayment(String recurringTypeString) {
		// Ensure analog payments have "one time" recurring set

		SharedPreferences sharedPref = getSharedPreferences(
				getString(R.string.preference_file_key), Context.MODE_PRIVATE);
		Editor editor = sharedPref.edit();
		editor.putString("recurring", recurringTypeString);
		editor.commit();
		Log.e("recurrin", "recurring type :"+recurringTypeString);
		if( isAnalogPaymentType() && ( !recurringTypeString.equals(RECURRING_ONE_TIME)) )
		{
			notifyUser( getString( R.string.DONATION_RECURRING_WITH_ANALOG_PAYMENT ) );
			return;
		}
		RadioGroup paymentTypeRadioGroup = (RadioGroup) findViewById(R.id.radiogroup_payment_method);

		int selectedPaymentMethod = paymentTypeRadioGroup.getCheckedRadioButtonId();

		// Electronic check
		if( paymentMethod.equalsIgnoreCase(getString(R.string.electronic_check)) )
		{
			saveProductToCart( recurringTypeString, false, false );						
			
			new AlertDialog.Builder(this)
	        .setTitle("Thanks for joining our automatic " + recurringTypeString.toLowerCase() + " giving program.")
	        .setMessage("To enroll, please fill out your checking account and routing number " +
	        		"on the next screen and provide the canvasser with a voided check.  " +
	        		"Joining one of our automatic EFT giving programs does not require you to " +
	        		"write a one-time check.")
	        .setNeutralButton("Continue", new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int which) { 
	            	//startACHActivity();



					startActivity( FundACHActivity.class );
	            }
	         })
	         .show();
		}
		// Paper check
		else if( paymentMethod.equalsIgnoreCase(getString(R.string.radio_method_analog_check)) )
		{
			saveProductToCart( recurringTypeString, true, false );

			Question occupationQuestion = petition.getQuestionWithVarName(FundQuestions.OCCUPATION);
			Question employerQuestion = petition.getQuestionWithVarName(FundQuestions.EMPLOYER);
			
			if(recurringType.equalsIgnoreCase(RECURRING_ONE_TIME)){
				startActivity( getNextActivityAfterActivityClass(FundDonationActivity.class) );
			}else{
				if(occupationQuestion != null && employerQuestion != null)
				{
					startActivity( FundOccupationEmployerActivity.class );
				}
				else
				{
					startActivity( getNextActivityAfterActivityClass(FundDonationActivity.class) );
				}  
			}
			
			
		}
		// Credit card
		else if( paymentMethod.equalsIgnoreCase(getString(R.string.radio_method_creditcard)))
		{
			saveProductToCart( recurringTypeString, false, false );
			//startCreditCardActivity();
			startActivity( FundSwipeCardActivity.class );
		}
		// Cash
		else if( paymentMethod.equalsIgnoreCase(getString(R.string.radio_method_cash)) )
		{
			saveProductToCart( recurringTypeString, false, true );
			startActivity( getNextActivityAfterActivityClass(FundDonationActivity.class) );

			Question occupationQuestion = petition.getQuestionWithVarName(FundQuestions.OCCUPATION);
			Question employerQuestion = petition.getQuestionWithVarName(FundQuestions.EMPLOYER);

			/*
			if(donationAmount > 25 && donationAmount < 125){
				startActivity( PACAbove25Activty.class );
			}else if(donationAmount > 125 || donationAmount < 25){
				startActivity( getNextActivityAfterActivityClass(FundDonationActivity.class) );
			}
			else{
				if(occupationQuestion != null && employerQuestion != null)
				{
					startActivity( FundOccupationEmployerActivity.class );
				}
				else
				{
					startActivity( getNextActivityAfterActivityClass(FundDonationActivity.class) );
				}  
			}
			*/
			
			 

		}
		// None selected, notify user
		else
		{
			notifyUser( getString( R.string.DONATION_SELECT_PAYMENT_METHOD_MESSAGE ) );
		}
	}

	public void startCreditCardActivity() {
		Intent intent = new Intent(FundDonationActivity.this, FundSwipeCardActivity.class);
		startActivity(intent);

	}

	public void startACHActivity() {
		Intent intent = new Intent(FundDonationActivity.this, FundACHActivity.class);
		startActivity(intent);
	}



	@Override
	protected void onNext() {
		String firstnameShipping = getSubject().getFirstName();
		String lastNameShipping = getSubject().getLastName();
		String addressShipping = getSubject().getAddressLine1();
		String address2shiiping = getSubject().getAddressLine2();
		String cityShippping = getSubject().getCity();
		String stateShipping = getSubject().getState();
		String zipShipping = getSubject().getZip();

		Question firstnameShippingQuestion = petition.getQuestionWithVarName(Question.FIRST_NAME+Question.SHIPPING_POSTSCRIPT);
		petitionAnswers.overwriteAnswer(new SubjectAnswer(firstnameShippingQuestion.getId(), firstnameShipping));

		Question lastnameShippingQuestion = petition.getQuestionWithVarName(Question.LAST_NAME+Question.SHIPPING_POSTSCRIPT);
		petitionAnswers.overwriteAnswer(new SubjectAnswer(lastnameShippingQuestion.getId(), lastNameShipping));

		Question addressShippingQuestion = petition.getQuestionWithVarName(Question.ADDRESS+Question.SHIPPING_POSTSCRIPT);
		petitionAnswers.overwriteAnswer(new SubjectAnswer(addressShippingQuestion.getId(), addressShipping));

		Question address2ShippingQuestion = petition.getQuestionWithVarName(Question.ADDRESS_2+Question.SHIPPING_POSTSCRIPT);
		petitionAnswers.overwriteAnswer(new SubjectAnswer(address2ShippingQuestion.getId(), address2shiiping));

		Question cityShippingQuestion = petition.getQuestionWithVarName(Question.CITY+Question.SHIPPING_POSTSCRIPT);
		petitionAnswers.overwriteAnswer(new SubjectAnswer(cityShippingQuestion.getId(), cityShippping));

		Question stateShippingQuestion = petition.getQuestionWithVarName(Question.STATE+Question.SHIPPING_POSTSCRIPT);
		petitionAnswers.overwriteAnswer(new SubjectAnswer(stateShippingQuestion.getId(), stateShipping));

		Question zipShippingQuestion = petition.getQuestionWithVarName(Question.ZIP+Question.SHIPPING_POSTSCRIPT);
		petitionAnswers.overwriteAnswer(new SubjectAnswer(zipShippingQuestion.getId(), zipShipping));
		// Validate subject information, allow anonymous donations (no subject information)
		if( isDigitalPaymentType() && missingSubjectInformation() )
		{
			notifyUser( getString( R.string.INVALID_SUBJECT_INFO_NOTIFICATION ) );
			return;
		}


		// Validate donation amount
		if( 0 == donationAmount )
		{
			notifyUser( getString( R.string.DONATION_ENTER_AMOUNT_MESSAGE ) );
			return;
		}

		//        // Validate donation frequency
		//        int recurringButtonId = recurringRadioGroup.getCheckedRadioButtonId();
		//        String recurringTypeString = recurringIdsToTypesMap.get( recurringButtonId );
		//        if( missingFrequency( recurringButtonId, recurringTypeString ) )
		//        {
		//            notifyUser( getString( R.string.DONATION_SELECT_RECURRING_MESSAGE ) );
		//            return;
		//        }



		if(recurringType == null){
			notifyUser( getString( R.string.DONATION_SELECT_RECURRING_MESSAGE ) );
			return;
		}

		if(paymentMethod == null){
			notifyUser( getString( R.string.DONATION_SELECT_PAYMENT_METHOD_MESSAGE ) );
			return;
		}
		// Handle payment
		handlePayment( recurringType );

	}
	
	/**
	 * Gets product information for the recurring type specified.
	 *
	 * @param recurringTypeString string representing the selected frequency
	 *
	 * @return a product corresponding to the reoccurring type
	 */
	@Override
	protected Product getProductFromString (String recurringTypeString)
	{
		List<Product> productsList = petition.getProducts();
		for( Product product : productsList )
		{
			if( product.getRecurring().equalsIgnoreCase( recurringTypeString ))
			{
				return product;
			}
		}
		return null;
	}

	void setRecurring(String recurring){
		this.recurringType = recurring;
	}

	void setPaymentMethod(String paymentMethod){
		this.paymentMethod = paymentMethod;
	}

	@Override
	protected boolean isAnalogPaymentType() {
		return isAnalog;

	}

	void setisAnalog(boolean isAnalog){
		this.isAnalog = isAnalog;
	}



	protected void updateAmount(){
		RadioGroup paymentAmountRadioGroup = (RadioGroup) findViewById(R.id.radiogroup_payment_amount);
		EditText amountEditText = (EditText) findViewById(R.id.edit_text_payment_amount);

		int checkedRadioButtonId = paymentAmountRadioGroup.getCheckedRadioButtonId();
		if (checkedRadioButtonId > 0) {
			RadioButton checkedPaymentAmountRadioButton = (RadioButton)paymentAmountRadioGroup.findViewById(checkedRadioButtonId);
			donationAmount = Float.parseFloat(checkedPaymentAmountRadioButton.getTag().toString());
		}
		if(donationAmount == 0){
			//amountEditText.setEnabled(true);
			amountEditText.setFocusableInTouchMode(true);
			amountEditText.setFocusable(true);
			amountEditText.requestFocus();
			amountEditText.requestFocusFromTouch();
			//             if(amountEditText.requestFocus()) {
			//            	    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
			//            	}
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.showSoftInput(amountEditText, InputMethodManager.SHOW_IMPLICIT);

		}else{
			amountEditText.clearFocus();
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			View view = this.getCurrentFocus();
			if (view != null) {
				imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
			}
			amountEditText.setFocusable(false);
			amountEditText.clearComposingText();
		}
	}

	protected void updateMethod(){
		RadioGroup paymentTypeRadioGroup = (RadioGroup) findViewById(R.id.radiogroup_payment_method);

		int checkedRadioButtonId = paymentTypeRadioGroup.getCheckedRadioButtonId();
		if (checkedRadioButtonId > 0) {
			RadioButton checkedPaymentTypeRadioButton = (RadioButton)paymentTypeRadioGroup.findViewById(checkedRadioButtonId);
			setPaymentMethod(checkedPaymentTypeRadioButton.getText().toString());
			int selectedPaymentType = paymentTypeRadioGroup.getCheckedRadioButtonId();
			if (selectedPaymentType == R.id.radiobutton_payment_method_cash ||
					selectedPaymentType == R.id.radiobutton_payment_method_paper_check )
			{
				setisAnalog(true);
			}else{
				if(recurringType != null && recurringType.equalsIgnoreCase(RECURRING_MONTHLY) && selectedPaymentType == R.id.radiobutton_payment_method_checking){
					setPaymentMethod(getString(R.string.electronic_check));
				}
				setisAnalog(false);
			}

		}
	}


}