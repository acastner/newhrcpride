package com.grassroots.funds.activities;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.grassroots.funds.R;
import com.grassroots.petition.activities.BasePetitionActivity;
import com.grassroots.petition.models.Petition;
import com.grassroots.petition.models.Subject;
import com.grassroots.petition.views.SubjectInfoView;

public class FundEmailActivity extends BasePetitionActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.email);
		TextView headerTitle = (TextView) findViewById(R.id.header_title);
		if (null != headerTitle) {
			headerTitle.setText(getString(R.string.phoneemail));
		}

		Subject subject = getSubject();
		
		EditText emailField = (EditText) findViewById(R.id.email_edit);
		emailField.setText(SubjectInfoView.subjectEmail);
		
		EditText homeField = (EditText) findViewById(R.id.home_edit);
		homeField.setText(SubjectInfoView.subjectHomePhone);
		
		EditText cellField = (EditText) findViewById(R.id.cell_edit);
		cellField.setText(SubjectInfoView.subjectCellPhone);

		PhoneNumberFormattingTextWatcher watcher = new PhoneNumberFormattingTextWatcher();
		homeField.addTextChangedListener(watcher);
		cellField.addTextChangedListener(watcher);

		homeField.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE) {
					onNext();
				}
				return false;
			}

		});

		Button nextButton = (Button) findViewById(R.id.next_button);
		nextButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				onNext();
			}
		});

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}
	private String removeNonDigits(String text) {
		text = text.replaceAll("\\(", "");
		text = text.replaceAll("-", "");
		text = text.replaceAll("\\)", "");
		text = text.replaceAll("\\s", "");
		return text;
	}

	protected void onNext() {
		EditText emailField = (EditText) findViewById(R.id.email_edit);
		EditText homeField = (EditText) findViewById(R.id.home_edit);
		EditText cellField = (EditText) findViewById(R.id.cell_edit);

		String email = emailField.getText().toString().trim();
		String home = homeField.getText().toString().trim();
		home = removeNonDigits(home);
		String cell = cellField.getText().toString().trim();
		cell = removeNonDigits(cell);



		if (cell.length() >0 && cell.length() < 10) {
			Toast.makeText(FundEmailActivity.this,
					"Please enter a valid cell phone number",
					Toast.LENGTH_LONG).show();
			return;
		}

		if (home.length() > 0 && home.length() < 10) {
			Toast.makeText(FundEmailActivity.this,
					"Please enter a valid home phone number",
					Toast.LENGTH_LONG).show();
			return;
		}


		Subject subject = getSubject();
		subject.setEmail(email);
		subject.setHomePhone(home);
		subject.setCellPhone(cell);
		subject.setPhone("");
		startActivity(getNextActivity());

	}

	public static boolean shouldProceedWithPetition(Petition petition) {
		return true;
	}
}
