package com.grassroots.funds.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import ch.qos.logback.core.status.OnConsoleStatusListener;
import android.widget.TextView.OnEditorActionListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.grassroots.funds.R;

import com.grassroots.funds.models.FundQuestions;
import com.grassroots.petition.activities.BaseSubjectInfoActivity;
import com.grassroots.petition.fragments.DatePickerDialogFragment;
import com.grassroots.petition.models.Question;
import com.grassroots.petition.models.QuestionAnswer;
import com.grassroots.petition.models.Subject;
import com.grassroots.petition.models.SubjectAnswer;
import com.grassroots.petition.views.SubjectInfoView;
import com.grassroots.petition.views.SubjectInformationButtons;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FundSubjectInfoActivity extends BaseSubjectInfoActivity implements DatePickerDialogFragment.OnSetDateListener {
	private Button poBoxAddressButton,partnerInfoButton,endInterviewButton;
	private Question mobileActionQn;
	CheckBox mobileActionCheck;
	Map<String, String> countryMapings, stateMapings;
	private Spinner countrySpinner, stateSpinner;
	private boolean firstSelection = true;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

//		poBoxAddressButton = (Button)  findViewById(R.id.pobox_button);
//		poBoxAddressButton.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				SubjectInformationButtons buttonsView = (SubjectInformationButtons) findViewById(R.id.button_panel);
//				SubjectInfoView enclosingview = buttonsView.getEnclosingView();
//				setSubject(enclosingview.getSubject());
//				if(validateFields()){
//					Intent intent = new Intent(FundSubjectInfoActivity.this,FundPOBoxActivity.class);
//					startActivity(intent);
//				}
//
//			}
//		});
//		partnerInfoButton = (Button) findViewById(R.id.partnerinfo_button);
//		partnerInfoButton.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) { 
//				SubjectInformationButtons buttonsView = (SubjectInformationButtons) findViewById(R.id.button_panel);
//				SubjectInfoView enclosingview = buttonsView.getEnclosingView();
//				setSubject(enclosingview.getSubject());
//				if(validateFields()){
//					Intent intent = new Intent(FundSubjectInfoActivity.this,FundPartnerInfoActivity.class);
//					startActivity(intent);
//				}
//			}
//		});

//		endInterviewButton = (Button) findViewById(R.id.end_interview_button);
//		endInterviewButton.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				Intent intent = new Intent(FundSubjectInfoActivity.this,FundTerminationActivity.class);
//				startActivity(intent);				
//			}
//		});

//		if(petition != null && petition.getAppThemeColor() != null)
//		{
//			poBoxAddressButton.setBackgroundColor(Color.parseColor("#" + petition.getAppThemeColor()));
//			partnerInfoButton.setBackgroundColor(Color.parseColor("#" + petition.getAppThemeColor()));
//
//		}
//		


		//EMAIL SECTION
		EditText emailField = (EditText) findViewById(R.id.email_edit);
		//emailField.setText(SubjectInfoView.subjectEmail);

		EditText homeField = (EditText) findViewById(R.id.home_edit);
		//homeField.setText(SubjectInfoView.subjectHomePhone);

		EditText cellField = (EditText) findViewById(R.id.cell_edit);
		//cellField.setText(SubjectInfoView.subjectCellPhone);

		PhoneNumberFormattingTextWatcher watcher = new PhoneNumberFormattingTextWatcher();
		homeField.addTextChangedListener(watcher);
		cellField.addTextChangedListener(watcher);

		mobileActionQn = petition.getQuestionWithVarName(FundQuestions.MOBILE_ACTION);

		RelativeLayout mobileActionLayout = (RelativeLayout) findViewById(R.id.mobileactionlayout);

		if (mobileActionQn != null) {

			mobileActionCheck = (CheckBox) findViewById(R.id.mobileActionCheckBox);
			mobileActionCheck.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					if (isChecked) {
						if (!FundSubjectInfoActivity.this.getCellConfirmedAsValidMobileNumber()) {
							new AlertDialog.Builder(FundSubjectInfoActivity.this)
									.setTitle("Please enter a valid cell. ")
									.setMessage("Thanks for joining Mobile Action Network. We need a valid cell phone inorder to add you. Please enter a valid cell. ")
									.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog, int which) {
											mobileActionCheck.setChecked(false);
											return;
										}
									}).show();

						}
					}
				}
			});

		} else {
			mobileActionLayout.setVisibility(View.GONE);
		}
		

		/*homeField.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE) {
					onNext();
				}
				return false;
			}

		});*/


		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		SpannableString ss = new SpannableString(getResources().getString(R.string.mobile_action_agree));
		ClickableSpan clickableSpan = new ClickableSpan() {
			@Override
			public void onClick(View textView) {
				startActivity(new Intent(FundSubjectInfoActivity.this, FundMobileActionTermsActivity.class));
			}

			@Override
			public void updateDrawState(TextPaint ds) {
				super.updateDrawState(ds);
				ds.setUnderlineText(true);
			}
		};

		String text = getResources().getString(R.string.mobile_action_agree);
		String wordToFind = "See Terms and Conditions";
		ss.setSpan(clickableSpan, text.indexOf(wordToFind), text.indexOf(wordToFind) + wordToFind.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

		TextView textView = (TextView) findViewById(R.id.mobActionterms);
		textView.setText(ss);
		textView.setMovementMethod(LinkMovementMethod.getInstance());
		textView.setHighlightColor(Color.BLUE);
		LinearLayout countryLayout = (LinearLayout) findViewById(R.id.countryLayout);
		countrySpinner = (Spinner) findViewById(R.id.country_spinner);
		stateSpinner = (Spinner) findViewById(R.id.state_spinner);
		Question country = petition.getQuestionWithVarName("COUNTRY");
		if(country == null)
			countryLayout.setVisibility(View.GONE);

		populateCountryDropdowns();
		Button backbutton = (Button) findViewById(R.id.back_button);
		backbutton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();

			}
		});

		DatePickerDialogFragment datePickerDialogFragment = new DatePickerDialogFragment();

		Question dateOfBirth = petition.getQuestionWithVarName("DATE_OF_BIRTH");
		if(dateOfBirth != null)
		{
			EditText dob_edit = (EditText) findViewById(R.id.dob_edit);
			dob_edit.setVisibility(View.VISIBLE);
			dob_edit.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					datePickerDialogFragment.show(getSupportFragmentManager(), "datePicker");
				}
			});
		}
	}
	private void populateCountryDropdowns() {
		// grab lists from petition json
		JSONObject jsonObj = petition.getPetitionJSON();
		JSONArray jsonArr = new JSONArray();

		try {
			jsonArr = jsonObj.getJSONArray("ScriptProperties");

			for (int i = 0; i < jsonArr.length(); i++) {
				if (jsonArr.getJSONObject(i).getString("Name")
						.equals("combolist")) {
					JSONObject jsonObj2 = new JSONObject(jsonArr.getJSONObject(
							i).getString("Value"));
					if (jsonObj2.has("countries")) {
						populateCountrySpinners(jsonObj2
								.getJSONArray("countries"));
					}
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}
	private void populateCountrySpinners(JSONArray jsonArray) {
		// store mapings in format - Map<Country name, Country code>
		countryMapings = new HashMap<String, String>();

		ArrayList<String> countryArray = new ArrayList<String>();

		for (int i = 0; i < jsonArray.length(); i++) {
			try {
				countryArray
						.add((jsonArray.getJSONObject(i).getString("item")));
				countryMapings.put(
						(jsonArray.getJSONObject(i).getString("item")),
						(jsonArray.getJSONObject(i).getString("code")));
				if(countryArray.size() == 1)
					populateStateDropdowns(jsonArray.getJSONObject(i).getString("code"));

			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		countrySpinner.setPrompt("Country");
		countrySpinner.setAdapter(new ArrayAdapter<String>(this,
				R.layout.spinner_layout, countryArray));
		countrySpinner.setSelection(0);
		stateSpinner.setPrompt("State");
		countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
									   int position, long id) {

				String country = countrySpinner.getSelectedItem().toString();
				String code = countryMapings.get(country);
				Question countryQ = petition.getQuestionWithVarName("COUNTRY");
				if (country.length() > 0 && countryQ != null) {
					petitionAnswers.overwriteAnswer(new SubjectAnswer(countryQ
							.getId(), code));
				}
				if (code.equalsIgnoreCase("US") || code.equalsIgnoreCase("CA")) {
					stateSpinner.setVisibility(View.VISIBLE);
					stateSpinner.setPrompt("State");
					Question stateQ = petition.getQuestionWithVarName("STATE");
					petitionAnswers.overwriteAnswer(new SubjectAnswer(stateQ
							.getId(), ""));

					populateStateDropdowns(code);
					if (!firstSelection) {
						stateSpinner.performClick();
					}
				} else {
					stateSpinner.setVisibility(View.GONE);

				}

				firstSelection = false;
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}

		});
	}
	private void populateStateDropdowns(String countryCode) {
		// grab lists from petition json
		JSONObject jsonObj = petition.getPetitionJSON();
		JSONArray jsonArr = new JSONArray();
		JSONArray statesToShow = new JSONArray();
		try {
			jsonArr = jsonObj.getJSONArray("ScriptProperties");

			for (int i = 0; i < jsonArr.length(); i++) {
				if (jsonArr.getJSONObject(i).getString("Name")
						.equals("combolist")) {
					JSONObject jsonObj2 = new JSONObject(jsonArr.getJSONObject(
							i).getString("Value"));
					if (jsonObj2.has("states")) {
						JSONArray allStatesArray = jsonObj2
								.getJSONArray("states");
						for (int j = 0; j < allStatesArray.length(); j++) {
							if (countryCode.equalsIgnoreCase(allStatesArray
									.getJSONObject(j).getString("country"))) {
								statesToShow.put(allStatesArray
										.getJSONObject(j));
							}
						}
						populateStateSpinners(statesToShow);
					}
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}
	private void populateStateSpinners(JSONArray jsonArray) {
		// store mapings in format - Map<Country name, Country code>
		stateMapings = new HashMap<String, String>();

		ArrayList<String> stateArray = new ArrayList<String>();

		for (int i = 0; i < jsonArray.length(); i++) {
			try {
				stateArray.add((jsonArray.getJSONObject(i).getString("item")));
				stateMapings.put(
						(jsonArray.getJSONObject(i).getString("item")),
						(jsonArray.getJSONObject(i).getString("code")));

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		stateSpinner.setAdapter(new ArrayAdapter<String>(this,
				R.layout.spinner_layout, stateArray));
		stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
									   int position, long id) {
				String state = stateSpinner.getSelectedItem().toString();
				String code = stateMapings.get(state);
				Question stateQ = petition.getQuestionWithVarName("STATE");
				if (state.length() > 0 && stateQ != null) {
					petitionAnswers.overwriteAnswer(new SubjectAnswer(stateQ
							.getId(), code));
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}

		});

	}
	@Override
	public void onBackPressed() {
		finish();
	}

	private String removeNonDigits(String text) {
		text = text.replaceAll("\\(", "");
		text = text.replaceAll("-", "");
		text = text.replaceAll("\\)", "");
		text = text.replaceAll("\\s", "");
		return text;
	}
	
	public void onResume() {
		super.onResume();
		
		//setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}
	
	@Override
	protected void goToNext(Subject subject) {
		
		
		EditText emailField = (EditText) findViewById(R.id.email_edit);
		EditText homeField = (EditText) findViewById(R.id.home_edit);
		EditText cellField = (EditText) findViewById(R.id.cell_edit);
		EditText dobField = (EditText) findViewById(R.id.dob_edit);

		String email = emailField.getText().toString().trim();
		String home = homeField.getText().toString().trim();
		home = removeNonDigits(home);
		String cell = cellField.getText().toString().trim();
		cell = removeNonDigits(cell);
		String dob = dobField.getText().toString().trim();

		if(dob.length() <= 0 && dobField.getVisibility() == View.VISIBLE)	{
			Toast.makeText(this, "Please enter Date of Birth", Toast.LENGTH_LONG).show();
			return;
		}

		if (cell.length() >0 && cell.length() < 10) {
			Toast.makeText(this,
					"Please enter a valid cell phone number",
					Toast.LENGTH_LONG).show();
			return;
		}

		if (home.length() > 0 && home.length() < 10) {
			Toast.makeText(this,
					"Please enter a valid home phone number",
					Toast.LENGTH_LONG).show();
			return;
		}

		if(mobileActionQn != null) {
			List<QuestionAnswer> answers = mobileActionQn.getAnswers();
			QuestionAnswer questionAnswer = null;
			if (mobileActionCheck.isChecked()) {
				questionAnswer = answers.get(0);
			} else {
				if (answers.size() > 1) {
					questionAnswer = answers.get(1);
				}
			}

			SubjectAnswer answer = new SubjectAnswer();
			answer.setQuestionId(mobileActionQn.getId());
			answer.setAnswerId(questionAnswer.getId());

			petitionAnswers.overwriteAnswer(answer);
		}
	    
		

		//Subject subject = getSubject();
		subject.setEmail(email);
		subject.setHomePhone(home);
		subject.setCellPhone(cell);
		subject.setPhone("");
		
		super.goToNext(subject);
	}

	@Override
	public void onSetDate(Calendar calendar) {
		Calendar dobCal = Calendar.getInstance();
		Calendar presentDay = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy MM");
		SimpleDateFormat systemDOBFormat = new SimpleDateFormat("MM/yy");

		dobCal.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
		int age = presentDay.get(Calendar.YEAR) - dobCal.get(Calendar.YEAR);
		if(presentDay.get(Calendar.DAY_OF_YEAR) < dobCal.get(Calendar.DAY_OF_YEAR))
			age--;

		if(age >= 18) {
			EditText dob_edit = (EditText) findViewById(R.id.dob_edit);
			dob_edit.setText(dateFormat.format(dobCal.getTime()));
			Question dobQuestion = petition.getQuestionWithVarName("DATE_OF_BIRTH");
			SubjectAnswer answer = new SubjectAnswer();
			answer.setStringAnswer(systemDOBFormat.format(dobCal.getTime()));
			answer.setQuestionId(dobQuestion.getId());
			petitionAnswers.overwriteAnswer(answer);
		}
		else	{
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
			alertDialog.setMessage("You must be at least 18 years old to continue.")
					.setPositiveButton("OK", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {
							//Do nothing
						}
					}).show();
		}
	}

}
