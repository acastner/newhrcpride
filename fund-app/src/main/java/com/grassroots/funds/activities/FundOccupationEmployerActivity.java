package com.grassroots.funds.activities;

import com.grassroots.funds.R;
import com.grassroots.funds.models.FundQuestions;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.grassroots.petition.activities.BasePetitionActivity;
import com.grassroots.petition.models.Question;
import com.grassroots.petition.models.Subject;
import com.grassroots.petition.models.SubjectAnswer;

public class FundOccupationEmployerActivity extends BasePetitionActivity{

	EditText occupationEdit;
	EditText employerEdit;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.occupation_employer);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		TextView headerTitle = (TextView) findViewById(R.id.header_title);
		ImageView logo = (ImageView) findViewById(R.id.header_logo);
		logo.setVisibility(View.GONE);

		headerTitle.setText("Occupation and Employer");

		Subject subject = getSubject();

		occupationEdit = (EditText) findViewById(R.id.occupation_edit);
		occupationEdit.setText(subject.getOccupation());

		employerEdit = (EditText) findViewById(R.id.employer_edit);
		employerEdit.setText(subject.getEmployer());

		Button nextButton = (Button) findViewById(R.id.next_button);
		nextButton.setOnClickListener(v -> onNext());
		
		Button backButton = (Button) findViewById(R.id.back_button);
		backButton.setOnClickListener(v -> onBackPressed());
	}

	protected void onNext() {

		Question occupationQuestion = petition.getQuestionWithVarName(FundQuestions.OCCUPATION);
		Question employerQuestion = petition.getQuestionWithVarName(FundQuestions.EMPLOYER);

		if((occupationQuestion.isRequired() && occupationEdit.getText().length() == 0)
				|| (employerQuestion.isRequired() && employerEdit.getText().length() == 0))
		{
			
			new AlertDialog.Builder(FundOccupationEmployerActivity.this)
			.setMessage("This information is required in order to donate to the Environment America Voter Action")
			.setNeutralButton("Ok", (dialog, which) -> {}).show();
			
			Toast.makeText(FundOccupationEmployerActivity.this, "Please fill out entire form.", Toast.LENGTH_LONG).show();
		}
		else
		{
			petitionAnswers.overwriteAnswer(new SubjectAnswer(occupationQuestion.getId(), occupationEdit.getText().toString()));		
			petitionAnswers.overwriteAnswer(new SubjectAnswer(employerQuestion.getId(), employerEdit.getText().toString()));
			
			startActivity(FundPledgeActivity.class);
		}

	}

	@Override
	public void onBackPressed() {

		try {
			petitionAnswers.removeAnswer(petition.getQuestionWithVarName(FundQuestions.OCCUPATION));
		} catch(Exception ex) {

		}
		try {
			petitionAnswers.removeAnswer(petition.getQuestionWithVarName(FundQuestions.EMPLOYER));
		} catch(Exception ex) {

		}
		super.onBackPressed();
	}
}
