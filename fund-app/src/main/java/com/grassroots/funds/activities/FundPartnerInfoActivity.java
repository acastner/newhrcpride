package com.grassroots.funds.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.grassroots.funds.R;
import com.grassroots.funds.models.FundQuestions;
import com.grassroots.petition.activities.BasePetitionActivity;
import com.grassroots.petition.models.Question;
import com.grassroots.petition.models.SubjectAnswer;

public class FundPartnerInfoActivity extends BasePetitionActivity {

@Override
public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.partner_info);
	 TextView headerTitle = (TextView) findViewById(R.id.header_title);
     if (null != headerTitle) {
         headerTitle.setText(getString(R.string.partnerinfo));
     }
	Button nextButton = (Button) findViewById(R.id.next_button);
	Button poBoxButton = (Button) findViewById(R.id.pobox_button);
	
	if(petition != null && petition.getAppThemeColor() != null)
	{
		poBoxButton.setBackgroundColor(Color.parseColor("#" + petition.getAppThemeColor()));
	}

	nextButton.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			Question partnerSalutationQuestion = petition.getQuestionWithVarName(FundQuestions.PARTNER_SALUTATION);
			TextView partnerSalutationView = (TextView) findViewById(R.id.partner_salutation_edit);
			if(partnerSalutationQuestion.isRequired() && partnerSalutationView.getText().toString().length()==0){
				Toast.makeText(FundPartnerInfoActivity.this, "Please enter partner salutation", Toast.LENGTH_LONG).show();
				return;
			}else{
			    petitionAnswers.overwriteAnswer(new SubjectAnswer(partnerSalutationQuestion.getId(), partnerSalutationView.getText().toString()));
			}
			
			Question partnerFirstNameQuestion = petition.getQuestionWithVarName(FundQuestions.PARTNER_FIRSTNAME);
			TextView partnerFirstNameView = (TextView) findViewById(R.id.partner_firstname_edit);
			
			if(partnerFirstNameQuestion.isRequired() && partnerFirstNameView.getText().toString().length()==0){
				Toast.makeText(FundPartnerInfoActivity.this, "Please enter partner first name", Toast.LENGTH_LONG).show();
				return;
			}else{
				petitionAnswers.overwriteAnswer(new SubjectAnswer(partnerFirstNameQuestion.getId(), partnerFirstNameView.getText().toString()));
			}
			
			Question partnerLastNameQuestion = petition.getQuestionWithVarName(FundQuestions.PARTNER_LASTNAME);
			TextView partnerLastNameView = (TextView) findViewById(R.id.partner_lastname_edit);
			if(partnerLastNameQuestion.isRequired() && partnerLastNameView.getText().toString().length()==0){
				Toast.makeText(FundPartnerInfoActivity.this, "Please enter partner lastname", Toast.LENGTH_LONG).show();
				return;
			}else{
				petitionAnswers.overwriteAnswer(new SubjectAnswer(partnerLastNameQuestion.getId(), partnerLastNameView.getText().toString()));
			}
			startActivity(getNextActivityAfterActivityClass(FundSubjectInfoActivity.class));
			
		}
	});
	
	poBoxButton.setOnClickListener(new OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent = new Intent(FundPartnerInfoActivity.this,FundPOBoxActivity.class);
			startActivity(intent);
		}
	});
}
}