package com.grassroots.funds.fragments;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;

import com.grassroots.funds.R;
import com.grassroots.petition.activities.BasePetitionActivity;
import com.grassroots.petition.activities.BaseWalklistActivity;
import com.grassroots.petition.adapters.LastNameListAdapter;
import com.grassroots.petition.fragments.*;
import com.grassroots.petition.fragments.WalklistListFragment.ViewListener;
import com.grassroots.petition.models.KnockStatus;
import com.grassroots.petition.models.Subject;
import com.grassroots.petition.models.Walklist;
import com.grassroots.petition.tasks.UpdateWalklistTask;
import com.grassroots.petition.utils.TypeFaceSetter;
import com.grassroots.petition.views.lists.WalklistListAdapter;

public class FundWalklistListFragment extends WalklistListFragment {
	
	private static final String TAG = WalklistListFragment.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);

    public static interface ViewListener {
        public void onSelectSubject(Subject subject);
        public void onFilterStreetName(String filter);
        public void onShowHouseNumber(Walklist.HouseNumberFilter filter);
        public void onFilterKnockStatus(KnockStatus knockStatus);
        public void onFilterLastName(String filter);
		/**
		 * Create a new subject record so they can be surveyed
		 */
		public void onNewRecord();
		/**
		 * 
		 * @param subject
		 */
		public void onSelectLastName(String lastname);
		
		
		public void  onTally();
    }

    private Walklist walklist = null;
    private Subject selectedSubject = null;

    protected ViewListener listener;
    private ListView listView;
    private LayoutInflater inflater;
    private Spinner filterSpinner;
    private Spinner filterKnockStatusSpinner;
    private Spinner lastNameSpinner;
    private Spinner filterColorSpinner;

    private ArrayAdapter<String> streetListAdapter;
    private ArrayAdapter<String> knockStatusListAdapter;
    private ArrayAdapter<String> lastNameListAdapter;
    private WalklistListAdapter listAdapter;
    private Button filterResetButton;
    private Button refreshWalklistButton;

    private String streetNameFilter = "";
    private Walklist.HouseNumberFilter houseNumberFilter = Walklist.HouseNumberFilter.ALL;
    private KnockStatus knockStatusFilter;
    private int filterPosition,knockFilterPosition,lastNameFilterPosition, colorFilterPosition;
    private ProgressDialog pd = null;
    private Button houseNumberToggle = null;
    private Walklist.HouseNumberFilter filter = null;
    private String houseNumberName = null;
    private String lastNameFilter = "";
    private boolean isLocationList,isTablet = false;
    
    private LastNameListAdapter lastnameAdapter;
    public ArrayList<String> lastNamesFromDatabase;
    private String selectedLastName;
    
    private int filtercount,knockfiltercount,lastnamefiltercount = 0;
    
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	super.onCreateView(inflater, container, savedInstanceState);        
        
        this.inflater = inflater;
        View rootView = inflater.inflate(R.layout.walklist_list_fragment, container, false);
       
        Context context = this.getActivity().getApplicationContext();
        pd = new ProgressDialog(context);
        if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) > Configuration.SCREENLAYOUT_SIZE_NORMAL) {
            // Assume Android 4.0, 7.0" tablet screen size
            isTablet = true;
        } else {
            // dart
            isTablet = false;
        }
        TypeFaceSetter.setRobotoFont(context, rootView);
        listView = (ListView) rootView.findViewById(R.id.walk_list);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            	if(isLocationList){
            	    String lastName = (String) adapterView.getItemAtPosition(i);
            	    selectedLastName = lastName;
            		listener.onSelectLastName(lastName);
            	}else{
            		//Subject subject = (Subject) adapterView.getItemAtPosition(i);
                    //listener.onSelectSubject(subject);
            	}
                
            }
        });
        

        filterSpinner = (Spinner) rootView.findViewById(R.id.filter_street_text_spinner);
        filterSpinner.setSelection(filterPosition);
        filterSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            	if(filtercount > 0){
                int selectedItemPosition = filterSpinner.getSelectedItemPosition();
                if (selectedItemPosition != 0) {
                	filterPosition = selectedItemPosition;
                    String selectedItemString = filterSpinner.getSelectedItem().toString();
                    listener.onFilterStreetName(selectedItemString);
                } else {
                    filterSpinner.setSelection(filterPosition);
                    listener.onFilterStreetName("");
                    filterSpinner.setSelection(filterPosition);

                }
            	}
            	filtercount++;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });

        lastNameSpinner = (Spinner) rootView.findViewById(R.id.last_name_spinner);
        lastNameSpinner.setSelection(lastNameFilterPosition);
        lastNameSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            	if(lastnamefiltercount > 0){
            	if(isLocationList && !(((BaseWalklistActivity)(getActivity())).getGlobalData().getLocation()==null)){
                int selectedItemPosition = lastNameSpinner.getSelectedItemPosition();
                if (selectedItemPosition != 0) {
                    lastNameFilterPosition = selectedItemPosition;
                    String selectedItemString = lastNameSpinner.getSelectedItem().toString();
                    listener.onFilterLastName(selectedItemString);
                } else {
                    listener.onFilterLastName("");
                    lastNameSpinner.setSelection(selectedItemPosition);

                }
            	}
            	}
            	lastnamefiltercount++;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });

        filterKnockStatusSpinner = (Spinner) rootView.findViewById(R.id.filter_knock_status_spinner);
        filterKnockStatusSpinner.setSelection(knockFilterPosition);

        filterKnockStatusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            	if(knockfiltercount > 0){
                int selectedItemPosition = filterKnockStatusSpinner.getSelectedItemPosition();
                if (selectedItemPosition != 0) {
                	knockFilterPosition = selectedItemPosition;
                	((BaseWalklistActivity)(getActivity())).getGlobalData().setKnockStatusFilter(KnockStatus.fromOrdinal(selectedItemPosition - 1).name());
                    ((BaseWalklistActivity)(getActivity())).getGlobalData().setKnockFilterPosition(knockFilterPosition);
                    listener.onFilterKnockStatus(KnockStatus.fromOrdinal(selectedItemPosition - 1));  
                } else {
                	((BaseWalklistActivity)(getActivity())).getGlobalData().setKnockStatusFilter(null);
                    ((BaseWalklistActivity)(getActivity())).getGlobalData().setKnockFilterPosition(0);
                	filterKnockStatusSpinner.setSelection(knockFilterPosition);
                    listener.onFilterKnockStatus(null);
                    
                }
            	}
            	knockfiltercount++;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });
        
        
//        filterColorSpinner = (Spinner) rootView.findViewById(R.id.filter_color_spinner);
//        filterColorSpinner.setSelection(colorFilterPosition);
//
//        filterColorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//            	if(knockfiltercount > 0){
//                int selectedItemPosition = filterKnockStatusSpinner.getSelectedItemPosition();
//                if (selectedItemPosition != 0) {
//                	knockFilterPosition = selectedItemPosition;
//                	((BaseWalklistActivity)(getActivity())).getGlobalData().setKnockStatusFilter(KnockStatus.fromOrdinal(selectedItemPosition - 1).name());
//                    ((BaseWalklistActivity)(getActivity())).getGlobalData().setKnockFilterPosition(knockFilterPosition);
//                    listener.onFilterKnockStatus(KnockStatus.fromOrdinal(selectedItemPosition - 1));  
//                } else {
//                	((BaseWalklistActivity)(getActivity())).getGlobalData().setKnockStatusFilter(null);
//                    ((BaseWalklistActivity)(getActivity())).getGlobalData().setKnockFilterPosition(0);
//                	filterKnockStatusSpinner.setSelection(knockFilterPosition);
//                    listener.onFilterKnockStatus(null);
//                    
//                }
//            	}
//            	knockfiltercount++;
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//                //To change body of implemented methods use File | Settings | File Templates.
//            }
//        });

        houseNumberToggle = (Button) rootView.findViewById(R.id.street_number_filter);
        if(houseNumberFilter != null){
            houseNumberToggle.setText(getResources().getString(houseNumberFilter.getDisplayTextId()));
        }
        houseNumberToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filter = getHouseNumberFilter().getNext();
                listener.onShowHouseNumber(filter);
                houseNumberToggle.setText(getResources().getString(filter.getDisplayTextId()));
            }
        });

        filterResetButton = (Button) rootView.findViewById(R.id.reset_all_filters);
        filterResetButton.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View view) {
            	if(filterSpinner.getVisibility()==View.VISIBLE){
                    setFilterPosition(0);
                    filterSpinner.setSelection(0);
            	}
                setKnockFilterPosition(0);
                filterKnockStatusSpinner.setSelection(0);
                if(!isLocationList && ((BaseWalklistActivity)(getActivity())).getGlobalData().getLocation() == null){
                Walklist.HouseNumberFilter filter = Walklist.HouseNumberFilter.ALL;
                setHouseNumberFilter(filter);
                setKnockStatusFilter(null);
                houseNumberToggle.setText(getResources().getString(filter.getDisplayTextId()));
                onSelectSubject(walklist.getSubject(0));
                }
                if(lastNameSpinner.getVisibility() == View.VISIBLE){
                	lastNameFilter = null;
                    lastNameFilterPosition = 0;
                    lastNameSpinner.setSelection(0);
                    knockStatusFilter = null;
                    ((BaseWalklistActivity)(getActivity())).getGlobalData().setLastNameFilter(lastNameFilter);
                    ((BaseWalklistActivity)(getActivity())).getGlobalData().setLastNameFilterPosition(lastNameFilterPosition);
                    ((BaseWalklistActivity)(getActivity())).getGlobalData().setKnockStatusFilter(null);
                    ((BaseWalklistActivity)(getActivity())).getGlobalData().setKnockFilterPosition(0);
                    setWalklist(walklist);
                    listener.onSelectLastName(lastNamesFromDatabase.get(0));
                }
              
            }
        });
        
        refreshWalklistButton = (Button) rootView.findViewById(R.id.refreshButton);
        refreshWalklistButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Activity activity = (BaseWalklistActivity) getActivity();;
                if(pd.isShowing()){
                    pd.dismiss();
                }
                pd = ProgressDialog.show(getActivity(),getString(R.string.pleasewaitmsg), getString(R.string.loadingmsg));
                UpdateWalklistTask walklistUpdateTask = new UpdateWalklistTask(((BasePetitionActivity)activity), ((BasePetitionActivity)activity).getGlobalData().getLastWalklistSyncTime(),FundWalklistListFragment.this, ((BasePetitionActivity)activity).getGlobalData().getLocation());
                walklistUpdateTask.execute();
            }
        });
		//configureNewRecordButton(rootView);
        //configureTallyRecordButton(rootView);
        configureMarketingMaterialsButton(rootView);
		if(selectedSubject != null){
		    onSelectSubject(selectedSubject);
		}
		if(((BasePetitionActivity)getActivity()).getGlobalData().getLocation() == null){
			isLocationList = false;
		}else{
			isLocationList = true;
		}
        return rootView;
    }
	
	
	/**
     * Configures marketing materials button.
     */
    private void configureMarketingMaterialsButton (View v)
    {
        v.findViewById( R.id.marketing_material_button ).setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                onMarketing();
            }
        } );
    }
    
//    /**
//	 * Configures new record button.
//	 */
//	private void configureNewRecordButton (View v)
//	{
//		v.findViewById( R.id.new_record_button ).setOnClickListener( new View.OnClickListener()
//		{
//			@Override
//			public void onClick (View view)
//			{
//				listener.onNewRecord();
//			}
//		} );
//		
//	}
//
//	private void configureTallyRecordButton(View v){
//	    Button tallyButton = (Button) v.findViewById(R.id.tally_button);
//	    if(isTablet){
//	        tallyButton.setVisibility(View.VISIBLE);
//	        tallyButton.setOnClickListener( new View.OnClickListener()
//	        {
//	            @Override
//	            public void onClick (View view)
//	            {
//	                listener.onTally();
//	            }
//	        } );
//	    }else{
//	        tallyButton.setVisibility(View.GONE);
//	    }
//	    
//	}
    
    /**
     * Marketing button tap handler
     */
    protected void onMarketing() 
    {
    	((BaseWalklistActivity)(getActivity())).getGlobalData().onMarketing(getActivity());
    }
    
    private Walklist.HouseNumberFilter getHouseNumberFilter() {
        String toggleDisplayText = houseNumberToggle.getText().toString();
        if (getResources().getString(R.string.all).equals(toggleDisplayText)) {
            return Walklist.HouseNumberFilter.ALL;
        } else if (getResources().getString(R.string.even).equals(toggleDisplayText)) {
            return Walklist.HouseNumberFilter.EVEN;
        } else if (getResources().getString(R.string.odd).equals(toggleDisplayText)) {
            return Walklist.HouseNumberFilter.ODD;
        }
        LOGGER.error("No matching resource for text: " + toggleDisplayText + ", showing all houses");
        return Walklist.HouseNumberFilter.ALL;
    }
    
    public void setViewListener(ViewListener listener) {
        this.listener = listener;
    }
    
}
