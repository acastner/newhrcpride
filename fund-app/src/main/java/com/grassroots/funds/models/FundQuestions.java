package com.grassroots.funds.models;

import com.grassroots.petition.models.Question;

public class FundQuestions extends Question{

	public static final String PLEDGE = "PLEDGE";
	public static final String GRASSROOTS = "GRASSROOTS";
	public static final String FUNDCONCLUSION = "CONCLUSION";
	public static final String SIGNATURE_CONSENT = "ACH_CONSENT";
	public static final String CREDIT_CARD_EXPIRATION_DATE = "CREDIT_CARD_EXPIRATION_DATE";
	public static final String CREDIT_CARD_TYPE = "CREDIT_CARD_TYPE";

    public static final String PAC_DISCLAIMER = "PAC_DISCLAIMER";

	public static final String FLAG_HEALTHCAREPROVIDER = "FLAG_HEALTHCAREPROVIDER";
	
	public static final String DATE_OF_BIRTH = "DATE_OF_BIRTH"; 
    public static final String OCCUPATION = "OCCUPATION";
    public static final String EMPLOYER = "EMPLOYER";
    public static final String EMPLOYER_CITY = "EMPLOYER_CITY";
    public static final String EMPLOYER_STATE = "EMPLOYER_STATE";
    
    public static final String POBOX = "POBOX";
    public static final String POBOX_CITY = "POBOX_CITY";
    public static final String POBOX_STATE = "POBOX_STATE";
    public static final String POBOX_ZIP = "POBOX_ZIP";
    
    public static final String PARTNER_FIRSTNAME = "PARTNER_FIRST_NAME";
    public static final String PARTNER_LASTNAME = "PARTNER_LAST_NAME";
    public static final String PARTNER_SALUTATION = "PARTNER_SALUTATION";
    public static final String PARTNER_FIRST_NAME = "PARTNER_FIRST_NAME";
    public static final String PARTNER_LAST_NAME = "PARTNER_LAST_NAME";
    public static final String PARTNER_EMAIL_ADDRESS = "PARTNER_EMAIL_ADDRESS";
    public static final String PARTNER_DATE_OF_BIRTH = "PARTNER_DATE_OF_BIRTH";
    
    public static final String MOBILE_ACTION = "MOBILE_ACTION_NETWORK";

    
	
    
}
