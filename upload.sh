echo "Starting build and upload script now!"

echo "***************************************"

echo "Cleaning previous builds"

./gradlew clean

echo "***************************************"
echo "Finished Cleaning"

if [ 0 -eq 1 ]; then
	echo "Building all FUND Apps"
	./gradlew assembleAdeRelease
	./gradlew assembleAfeRelease
	./gradlew assembleCaeRelease
	./gradlew assembleCapRelease
	./gradlew assembleCoeRelease
	./gradlew assembleCopRelease
	./gradlew assembleCteRelease
	./gradlew assembleCtpRelease
	./gradlew assembleGaeRelease
	./gradlew assembleFleRelease
	./gradlew assembleIleRelease
	./gradlew assembleIlpRelease
	./gradlew assembleMaeRelease
	./gradlew assembleMapRelease
	./gradlew assembleMdeRelease
	./gradlew assembleMdpRelease
	./gradlew assembleMeeRelease
	./gradlew assembleMieRelease
	./gradlew assembleMipRelease
	./gradlew assembleMneRelease
	./gradlew assembleNceRelease
	./gradlew assembleNcpRelease
	./gradlew assembleNjeRelease
	./gradlew assembleNjpRelease
	./gradlew assembleNmeRelease
	./gradlew assembleNmpRelease
	./gradlew assembleNyeRelease
	./gradlew assembleOheRelease
	./gradlew assembleOreRelease
	./gradlew assembleOrpRelease
	./gradlew assemblePaeRelease
	./gradlew assemblePapRelease
	./gradlew assembleRieRelease
	./gradlew assembleTacRelease
	./gradlew assembleTxeRelease
	./gradlew assembleTxpRelease
	./gradlew assembleUdpRelease
	./gradlew assembleUfpRelease
	./gradlew assembleVaeRelease
	./gradlew assembleWaeRelease
	./gradlew assembleWapRelease
	./gradlew assembleWieRelease
	./gradlew assembleWipRelease

	./gradlew assembleAzeRelease
	./gradlew assembleIaeRelease
	./gradlew assembleMoeRelease
	./gradlew assembleMteRelease
	./gradlew assembleFlfRelease
fi


if [ 0 -eq 1 ]; then
echo "Building HRC Canvass Apps"
./gradlew assemblePwasRelease
./gradlew assemblePmiaRelease
./gradlew assemblePdenRelease
./gradlew assemblePchiRelease
./gradlew assemblePatlRelease
./gradlew assemblePmanRelease
fi

if [ 0 -eq 1 ]; then
echo "Building Internal..."
./gradlew assembleInternalDebug
fi

for entry in "fund-app/build/outputs/apk"/*       #loop through all files in the output apk directory
do
	echo "$entry"                               #print the relative path of the file
	filename=$(basename "$entry")               #get only the filename
	echo "$filename"                            #print it
	IFS='_' read -r -a array <<< "$filename"    #split the filename using "_" as a delimiter 
	for element in "${array[@]}"                #print each of the split elements
	do
    	echo "$element"
	done

	s3path="s3://gru-apps/" 
	s3path+="${array[2]}"
	s3path+="/"
	s3path+="${array[3]}"
	s3path+="/"
	s3path+="${array[4]}"
	s3path+="/"
	s3path+="$filename"
	echo "$s3path"

	## now set up path for the latest release that remains a constant url
	s3pathLatest="s3://gru-apps-web/auto-uploaded/"
	s3pathLatest+="${array[2]}"
	s3pathLatest+="/"
	s3pathLatest+="${array[3]}"
	s3pathLatest+="/"
	s3pathLatest+="fund_latest.apk"
	echo "$s3pathLatest"

	###POST TO AUTO-UPDATER AREA###
	aws s3 cp "$entry" "$s3path"

	###POST TO WEB SITE DOWNLOADS###
	aws s3 cp "$entry" "$s3pathLatest" --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers

	#jsonPayload="payload={\"channel\": \"#release\", \"username\": \"AndroidReleaseBot\", \"text\": \""
	#jsonPayload+="<!channel> Fund Job 7 Version: "
	#jsonPayload+="(${array[3]}-"
	#jsonPayload+="${array[4]})"
	#jsonPayload+=" released via auto updater, and Fund downloads page. Latest release notes here: https://docs.google.com/document/d/1n6L69wUKI816ZxWlIwfHkplulUH6mdzQzWbMLdDzYQU/edit#"
	#jsonPayload+="\", \"icon_emoji\": \":banana_parrot:\"}"

	#echo "$jsonPayload"

	#if [ ! -z "${array[4]}" ]; then
	#	curl -X POST --data-urlencode "$jsonPayload" https://hooks.slack.com/services/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
	#fi


done