#!/bin/bash
#example bash command to upload to s3
#aws s3 cp README.md s3://gru-apps/com.grassroots.clitest/README.md


echo "Starting build and upload script now!"

echo "***************************************"

echo "Cleaning previous builds"

./gradlew clean

./gradlew assemblePmiaRelease
./gradlew assemblePdenRelease
./gradlew assemblePwasRelease
./gradlew assemblePchiRelease
./gradlew assemblePatlRelease
./gradlew assemblePmanRelease

if [ 0 -eq 1 ]; then
	echo "Building all"
	./gradlew assembleAdeRelease
	./gradlew assembleAfeRelease
	./gradlew assembleCaeRelease
	./gradlew assembleCapRelease
	./gradlew assembleCoeRelease
	./gradlew assembleCopRelease
	./gradlew assembleCteRelease
	./gradlew assembleCtpRelease
	./gradlew assembleGaeRelease
	./gradlew assembleIleRelease
	./gradlew assembleIlpRelease
	./gradlew assembleMaeRelease
	./gradlew assembleMapRelease
	./gradlew assembleMdeRelease
	./gradlew assembleMdpRelease
	./gradlew assembleMeeRelease
	./gradlew assembleMieRelease
	./gradlew assembleMipRelease
	./gradlew assembleMneRelease
	./gradlew assembleNceRelease
	./gradlew assembleNcpRelease
	./gradlew assembleNjeRelease
	./gradlew assembleNjpRelease
	./gradlew assembleNmeRelease
	./gradlew assembleNmpRelease
	./gradlew assembleNyeRelease
	./gradlew assembleOheRelease
	./gradlew assembleOreRelease
	./gradlew assembleOrpRelease
	./gradlew assemblePaeRelease
	./gradlew assemblePapRelease
	./gradlew assembleRieRelease
	./gradlew assembleTacRelease
	./gradlew assembleTxeRelease
	./gradlew assembleTxpRelease
	./gradlew assembleUdpRelease
	./gradlew assembleUfpRelease
	./gradlew assembleVaeRelease
	./gradlew assembleWaeRelease
	./gradlew assembleWapRelease
	./gradlew assembleWieRelease
	./gradlew assembleWipRelease
fi

#./gradlew assembleCtpRelease
#./gradlew assembleAdeRelease
#./gradlew assembleAfeRelease
#./gradlew assembleCaeRelease
#./gradlew assembleCapRelease
#./gradlew assembleCoeRelease
#./gradlew assembleCopRelease
#./gradlew assembleCteRelease
#./gradlew assembleGaeRelease
#./gradlew assembleIleRelease
#./gradlew assembleIlpRelease
#./gradlew assembleMaeRelease
#./gradlew assembleMapRelease
#./gradlew assembleMdeRelease
#./gradlew assembleMdpRelease
#./gradlew assembleMeeRelease
#./gradlew assembleMieRelease
#./gradlew assembleMipRelease
#./gradlew assembleMneRelease
#./gradlew assembleNceRelease
#./gradlew assembleNcpRelease
#./gradlew assembleNjeRelease
#./gradlew assembleNjpRelease
#./gradlew assembleNmeRelease
#./gradlew assembleNmpRelease
#./gradlew assembleNyeRelease
#./gradlew assembleOheRelease
#./gradlew assembleOreRelease
#./gradlew assembleOrpRelease
#./gradlew assemblePaeRelease
#./gradlew assemblePapRelease
#./gradlew assembleRieRelease
#./gradlew assembleTacRelease
#./gradlew assembleTxeRelease
#./gradlew assembleTxpRelease
#./gradlew assembleUdpRelease
#./gradlew assembleUfpRelease
#./gradlew assembleVaeRelease
#./gradlew assembleWaeRelease
#./gradlew assembleWapRelease
#./gradlew assembleWieRelease
#./gradlew assembleWipRelease       


echo "***************************************"
echo "Finished Cleaning"

#./gradlew assemblePavilionRelease


for entry in "fund-app/build/outputs/apk"/*       #loop through all files in the output apk directory
do
	echo "$entry"                               #print the relative path of the file
	filename=$(basename "$entry")               #get only the filename
	echo "$filename"                            #print it
	IFS='_' read -r -a array <<< "$filename"    #split the filename using "_" as a delimiter 
	for element in "${array[@]}"                #print each of the split elements
	do
    	echo "$element"
	done

	s3path="s3://gru-apps-web/archive/fund-job-7/7-24-2017/" 
	s3path+="${array[3]}"
	s3path+="/"
	s3path+="$filename"
	echo "$s3path"

	aws s3 cp "$entry" "$s3path" --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers


	#parts=$(echo $filename | tr "_" "\n")
	#for part in $parts
	#do
	#	echo "$part"
	#done
	#echo "$parts[0]"
done